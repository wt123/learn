package com.wt.vuedemo.entity;

/**
 * @description
 * @author: wangtao
 * @date:13:32 2018/9/20
 * @email:taow02@jumei.com
 */
public class TestEntity {
    private String age;
    private String name;

    public TestEntity() {
    }

    public TestEntity(String age, String name) {
        this.age = age;
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
