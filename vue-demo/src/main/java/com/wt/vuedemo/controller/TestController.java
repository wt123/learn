package com.wt.vuedemo.controller;

import com.wt.vuedemo.entity.TestEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description
 * @author: wangtao
 * @date:13:31 2018/9/20
 * @email:taow02@jumei.com
 */
@RestController
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/test")
    public TestEntity a(){
        return new TestEntity("wt","25");
    }
    @RequestMapping("/test1")
    public String a1(){
        return "ok";
    }
}
