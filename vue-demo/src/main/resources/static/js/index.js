var app;
window.onload = function (ev) {

    Vue.component('vuetr', {
        props: ['items'],
        methods: {
            showIndex: function (v) {
                alert(v);
            }
        },
        template: `<table border="1">
                        <thead>
                        <th>name</th>
                        <th>age</th>
                        <th>sex</th>
                        <th>operate</th>
                        </thead>
                        <tbody>
                        <tr class="classtr" v-bind:idx="index" v-for="(item,index) in items">
                            <td>{{ item.name }}</td>
                            <td>{{ item.age }}</td>
                            <td>{{ item.sex }}</td>
                            <td><button @click="showIndex('this index is ' + index)">查看</button></td>
                        </tr>
                        </tbody>
                    </table>`
    });

    app = new Vue({
        el: "#app",
        data: {
            message: 'hello Vue!',
            seen: true,
            todos: [{"name": "wt1", "age": "23", "sex": "1"}, {"name": "wt2", "age": "24", "sex": "2"}, {
                "name": "wt3",
                "age": "25",
                "sex": "3"
            }],
            tableItems: initTableItems()
        },
        methods: {
            sum: function (a, b) {
                alert(a + b);
                return a + b;
            },
            getText: function () {
                var table3 = document.getElementById("table3");
                alert(table3.innerHTML);
            },
            addTr:function () {
                this.tableItems.push('<tr><td>1</td><td>2</td><td><button onclick="sum(2,3)">查看</button></td>');
            },
            refreshTr: function () {
                this.tableItems = initTableItems();
            },
            changeTodosData: function (e){
                var idx = e.target.getAttribute("idx");
                var value = e.target.value;
                console.log(idx+":"+value);
                console.log(app._data.todos[idx]);
                app._data.todos[idx].name=value;
            },
            delTableItem: function (){
                var idx = this.$refs.delItem.value;
                app._data.tableItems.splice(idx,1);
                app._data.todos.splice(idx,1);
            }
        }
    });

};

function initTableItems() {
    return ['<tr><td>1</td><td>2</td><td><button onclick="sum(1,2)">查看</button></td>', '<tr><td>1</td><td>2</td><td>3</td></tr>', '<tr><td>1</td><td>2</td><td>3</td></tr>'];
}

function sum(a, b) {
    alert(a + ":" + b);
}

function getVueData() {
    alert(app._data);
    app.$options.methods.sum(100,100);
    app._data.tableItems.push('<tr><td>1</td><td>2</td><td><button onclick="sum(2,3)">查看</button></td>');
    app._data.todos.push({"name":"","age":"","sex":""});

}

function getTodosData() {
    app._data.todos.push({"name": "wt1", "age": "23", "sex": "1"});
}

function showTodosData() {
    console.log(app._data.todos);
}

//非vue管理的方法，要用dom的方式获取值
function changeTodoDataDom(obj){
    var idx = obj.getAttribute("idx");
    var value = obj.value;
    console.log(idx+":"+value);
    console.log(app._data.todos[idx]);
    app._data.todos[idx].name=value;
}

function changeTodoDataDom0(idx,value){
    //这个.nodeValue也可以直接写在input部分，不影响
    console.log(idx.nodeValue+":"+value);
    console.log(app._data.todos[idx]);
    app._data.todos[idx.nodeValue].name=value;
}