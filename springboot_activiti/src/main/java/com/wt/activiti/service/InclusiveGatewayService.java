package com.wt.activiti.service;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Description
 * @Author: wangtao
 * @Date:15:32 2017/6/30
 * @Email:tao8.wang@changhong.com
 */
@Service
public class InclusiveGatewayService {

    //注入为我们自动配置好的服务
    @Autowired
    private RuntimeService runtimeService;

    public void startProcess(String key, Map<String, Object> variables) {
        runtimeService.startProcessInstanceByKey(key, variables);
    }

    public void aaa(DelegateExecution execution) {
        System.out.println("method inclusiveGatewayService.aaa()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables:" + variables);
    }

    public void bbb(DelegateExecution execution) {
        System.out.println("method inclusiveGatewayService.bbb()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables:" + variables);
    }

    public void ccc(DelegateExecution execution) {
        System.out.println("method inclusiveGatewayService.ccc()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables:" + variables);
    }

    public void ddd(DelegateExecution execution) {
        System.out.println("method inclusiveGatewayService.ddd()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables:" + variables);
    }
}
