package com.wt.activiti.service;

import com.wt.activiti.response.TaskResoponse;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Description
 * @Author: wangtao
 * @Date:10:51 2017/7/7
 * @Email:tao8.wang@changhong.com
 */
@Service
public class VacationDemoService {

    //注入为我们自动配置好的服务
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private TaskService taskService;

    public void startProcess(String key, Map<String, Object> variables) {
        variables.put("processId", key);
        runtimeService.startProcessInstanceByKey(key, variables);
        System.out.println("process start");
    }

    //获得某个人的任务别表
    public List<TaskResoponse> getTasks(String assignee) {
        System.out.println("存在代理人的任务在其他人的任务列表是不可见的");
        List<Task> tasks = taskService.createTaskQuery().taskCandidateUser(assignee).list();
        System.out.println("候选人任务列表CandidateUser:" + tasks.size());
        if (tasks.size() == 0) {
            tasks = taskService.createTaskQuery().taskAssignee(assignee).list();
            System.out.println("代理人任务列表taskAssignee:" + tasks.size());
        }
        List<TaskResoponse> dtos = new ArrayList<TaskResoponse>();
        for (Task task : tasks) {
            dtos.add(new TaskResoponse(task.getId(), task.getName()));
        }
        return dtos;
    }

    //完成任务
    public Map<String, Object> completeTasks(Boolean flag, String taskId) {
        Map<String, Object> taskVariables = new HashMap<String, Object>();
        taskVariables.put("flag", flag);

        Map<String, Object> v = taskService.getVariables(taskId);
        v.put("flag", flag);

        taskService.complete(taskId, taskVariables);
        System.out.println("process complete");

        return v;
    }

    public void complete(String taskId) {
        taskService.complete(taskId);
    }

    public List<String> getManagers() {
        return Arrays.asList("mg1", "mg2", "mg3");
    }

    public void vacationResult(DelegateExecution execution) {
        Map<String, Object> variables = execution.getVariables();
        System.out.println(variables);
        if ((boolean) variables.get("flag"))
            System.out.println("vacation PASS");
        else
            System.out.println("vacation FAIL");
    }

    public void queryHistory() {
        System.out.println("hd query");
        List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery().processDefinitionKey("simpleProcess").list();
        for (HistoricProcessInstance hp : list) {
            System.out.println(hp.getProcessDefinitionId() + "---" + hp.getId());
            List<HistoricVariableInstance> list1 = historyService.createHistoricVariableInstanceQuery().processInstanceId(hp.getId()).list();
            List<HistoricActivityInstance> list2 = historyService.createHistoricActivityInstanceQuery().processInstanceId(hp.getId()).list();
            System.out.println("tasks:");
            for (HistoricActivityInstance ha : list2) {
                System.out.println("---" + ha.getActivityName() + ":" + ha.getActivityId() + ":" + ha.getAssignee());
            }
            System.out.println("variables:");
            for (HistoricVariableInstance hv : list1) {
                System.out.println("---" + hv.getVariableName() + ":" + hv.getValue());
            }
            System.out.println("on");
            List<Execution> list3 = runtimeService.createExecutionQuery().processInstanceId(hp.getId()).list();
            for (Execution e : list3) {
                System.out.println(e.getActivityId());
            }
            System.out.println("--------");
        }

    }

    public void claim(String taskId, String userId) {
        System.out.println("<" + userId + "> claim task <" + taskId + ">");
        taskService.claim(taskId, userId);
    }

    /////////////////////////////////////////////////////////////////////////////////////
    public void startProcess(String key) {
        Map<String, Object> variables = new HashMap<>();
        variables.put("processId", key);
        variables.put("days", 1);
        variables.put("pmChk", true);
        variables.put("mgChk", true);
        runtimeService.startProcessInstanceByKey(key, variables);
        System.out.println("process start");
    }

    public List<String> getPMs() {
        return Arrays.asList("pm");
    }

}
