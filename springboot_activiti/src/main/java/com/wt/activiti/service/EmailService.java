package com.wt.activiti.service;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Description
 * @Author: wangtao
 * @Date:10:20 2017/7/3
 * @Email:tao8.wang@changhong.com
 */
@Service
public class EmailService {
    //注入为我们自动配置好的服务
    @Autowired
    private RuntimeService runtimeService;

    public void startProcess(String key) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("recipient", "386427665@qq.com");
        variables.put("male", true);
        variables.put("orderId", "aa_orderId");
        variables.put("recipientName", "tom");
        variables.put("now", new Date());
        runtimeService.startProcessInstanceByKey(key, variables);
    }

    //邮件收件人列表用逗号隔开的字符串，list不行
    public String getToAddress() {
        System.out.println("getToAddress");
        return "386427665@qq.com";
    }

    public String getSubject(DelegateExecution execution) {
        System.out.println("getSubject");
        return "Your order " + execution.getVariable("orderId") + " has been shipped";
    }

    public String getHtml(DelegateExecution execution) {
        System.out.println("getHtml");
        Map<String, Object> variables = execution.getVariables();
        return "<html>\n" +
                "          <body>\n" +
                "          你好！ " + ((boolean) variables.get("male") ? "Mr." : "Mrs.") + variables.get("recipientName") + "<br/><br/>\n" +
                "          As of " + new Date() + ", your order has been <b>processed and shipped</b>.<br/><br/>\n" +
                "          Kind regards,<br/>\n" +
                "          TheCompany.\n" +
                "          </body>\n" +
                "          </html>";
    }
}