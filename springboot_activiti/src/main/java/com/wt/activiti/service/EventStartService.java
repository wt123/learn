package com.wt.activiti.service;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.BpmnError;
import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @Description
 * @Author: wangtao
 * @Date:8:27 2017/7/6
 * @Email:tao8.wang@changhong.com
 */
@Service
public class EventStartService {

    //注入为我们自动配置好的服务
    @Autowired
    private RuntimeService runtimeService;

    public void startProcess(String messageName) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("MessageName", messageName);
        //消息启动事件传入的key是message的Name，不是id
        //ProcessInstance startProcessInstanceByMessage(String messageName, Map<String, Object> processVariables);
        runtimeService.startProcessInstanceByMessage(messageName, variables);
    }

    public void startProcessByKey(String key) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("key", key);
        //消息启动事件传入的key是message的Name，不是id
        //ProcessInstance startProcessInstanceByMessage(String messageName, Map<String, Object> processVariables);
        runtimeService.startProcessInstanceByKey(key, variables);
    }

    public void messageEvent(DelegateExecution execution) {
        System.out.println("eventStartService.messageEvent()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println(variables);
    }

    public void errorEvent(DelegateExecution execution) {
        System.out.println("eventStartService.errorEvent()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println(variables);
        int i = new Random().nextInt(10);
        if (i < 5) {
            System.out.println("抛出异常，执行异常事件启动子流程");
            throw new BpmnError("MyErrorCode");
        } else {
            System.out.println("未抛出异常");
        }
    }

    //错误启动事件抛异常时执行
    public void errorSubProcess(DelegateExecution execution) {
        System.out.println("eventStartService.errorSubProcess()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println(variables);
    }

    //////////////////////////错误终止事件，边界事件/////////////////////////////////////////
    //设置flag得值，流程中有用到，为false的时候触发异常终止事件，并被边界事件捕获
    public void setFlag(DelegateExecution execution) {
        int i = new Random().nextInt(10);
        boolean b = true;
        if (i < 5)
            b = false;
        System.out.println("异常终止事件，边界事件：flag=" + b);
        execution.setVariable("flag", b);
    }

    //正常结束时servicetask执行
    public void noBoundary(DelegateExecution execution) {
        System.out.println("边界事件未执行：eventStartService.noBoundary()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println(variables);
    }

    //边界事件触发时执行
    public void boundary(DelegateExecution execution) {
        System.out.println("边界事件执行了：eventStartService.boundary()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println(variables);
    }
    //////////////////////////错误终止事件，边界事件/////////////////////////////////////////
}
