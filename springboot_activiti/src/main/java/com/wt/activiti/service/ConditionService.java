package com.wt.activiti.service;

import com.wt.activiti.response.TaskResoponse;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Description
 * @Author: wangtao
 * @Date:9:47 2017/6/30
 * @Email:tao8.wang@changhong.com
 */
@Service
public class ConditionService {

    //注入为我们自动配置好的服务
    @Autowired
    private RuntimeService runtimeService;

    public void startProcess(String key, Map<String, Object> variables) {
        runtimeService.startProcessInstanceByKey(key, variables);
    }

    public boolean returnBoolean() {
        int i = new Random().nextInt(10);
        boolean b = i < 5;
        System.out.println("conditionService.returnBoolean()--->:" + b);
        return b;
    }

    public void aaa(DelegateExecution execution) {
        System.out.println("method conditionService.aaa()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables:" + variables);
    }

    public void bbb(DelegateExecution execution) {
        System.out.println("method conditionService.bbb()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables:" + variables);
    }

}
