package com.wt.activiti.service;

import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.IdentityLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @Author: wangtao
 * @Date:13:23 2017/7/6
 * @Email:tao8.wang@changhong.com
 */
@Service
public class AuthorizationProcessService {

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private IdentityService identityService;

    @Autowired
    private RuntimeService runtimeService;

    public void startProcess(String key, String userId) {
        runtimeService.startProcessInstanceByKey(key);
    }

    public void queryProcessDefinition(String userId) {
        List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().startableByUser(userId).list();
        if (list.size() == 0) {
            System.out.println("find null processDefinition for user<" + userId + ">");
            return;
        }
        ProcessDefinition pd = list.get(0);
        System.out.println("<" + userId + ">processDefinitions:");
        System.out.println("---name:" + pd.getName());
        System.out.println("---id:" + pd.getId());
        System.out.println("---resourceName:" + pd.getResourceName());
        System.out.println("---key:" + pd.getKey());
        System.out.println("---category:" + pd.getCategory());
    }

    public void queryIdentityLink(String processId) {
        List<IdentityLink> links = repositoryService.getIdentityLinksForProcessDefinition(processId);
        if (links.size() == 0) {
            System.out.println("find null links for process<" + processId + ">");
            return;
        }
        for (IdentityLink link : links) {
            System.out.print("{groupId:" + link.getGroupId() + ",");
            System.out.print("processDefinitionId:" + link.getProcessDefinitionId() + ",");
            System.out.print("processInstanceId:" + link.getProcessInstanceId() + ",");
            System.out.print("taskId:" + link.getTaskId() + ",");
            System.out.print("type:" + link.getType() + ",");
            System.out.print("userId:" + link.getUserId() + "}");
            System.out.println("");
        }
    }

    public void queryAuthProcessUser(String processId) {
        List<User> list = identityService.createUserQuery().potentialStarter(processId).list();
        if (list.size() == 0) {
            System.out.println("find null users for process<" + processId + ">");
        } else {
            for (User user : list) {
                System.out.println("---email:" + user.getEmail());
                System.out.println("---firstName:" + user.getFirstName());
                System.out.println("---id:" + user.getId());
                System.out.println("---lastName:" + user.getLastName());
                System.out.println("---password:" + user.getPassword());
            }
        }
        List<Group> groups = identityService.createGroupQuery().potentialStarter(processId).list();
        if (groups.size() == 0) {
            System.out.println("find null groups for process<" + processId + ">");
            return;
        }
        for (Group g : groups) {
            System.out.println(g.getId());
            System.out.println(g.getName());
            System.out.println(g.getType());
        }
    }

}
