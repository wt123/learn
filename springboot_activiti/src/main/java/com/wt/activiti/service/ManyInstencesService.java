package com.wt.activiti.service;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author: wangtao
 * @Date:16:36 2017/7/3
 * @Email:tao8.wang@changhong.com
 */
@Service
public class ManyInstencesService {

    //注入为我们自动配置好的服务
    @Autowired
    private RuntimeService runtimeService;

    public void startProcess(String key) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("processId", key);
        runtimeService.startProcessInstanceByKey(key, variables);
    }

    public void aaa(DelegateExecution execution) {
        System.out.println("ManyInstencesService.aaa()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println(variables);

    }

    public List<String> getList() {
        System.out.println("ManyInstencesService.getList()");
        return Arrays.asList("value1","value2");
    }

    public List<String> getList2() {
        System.out.println("ManyInstencesService.getList2()");
        return Arrays.asList("value1","value2","value3");
    }

}
