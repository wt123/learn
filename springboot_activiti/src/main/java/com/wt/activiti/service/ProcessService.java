package com.wt.activiti.service;

import com.wt.activiti.response.TaskResoponse;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author: wangtao
 * @Date:9:50 2017/6/30
 * @Email:tao8.wang@changhong.com
 */
@Service
public class ProcessService {

    //注入为我们自动配置好的服务
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;

    public void startProcess(String key, Map<String, Object> variables) {
        if (variables != null) {
            System.out.println("start process <" + key + "> with variables");
            runtimeService.startProcessInstanceByKey(key, variables);
        } else {
            System.out.println("start process <" + key + "> without variables");
            runtimeService.startProcessInstanceByKey(key);
        }
    }

    //获得某个人的任务别表
    public List<Task> getTasks(String assignee) {
        List<Task> tasks = taskService.createTaskQuery().taskCandidateUser(assignee).list();
        return tasks;
    }

    public void complete(String taskId, Map<String, Object> variables) {
        if (variables != null) {
            System.out.println("complete task <" + taskId + "> with variables");
            taskService.complete(taskId, variables);
        } else {
            System.out.println("complete task <" + taskId + "> without variables");
            taskService.complete(taskId);
        }
    }
}
