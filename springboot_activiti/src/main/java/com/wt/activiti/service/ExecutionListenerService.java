package com.wt.activiti.service;

import org.activiti.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author: wangtao
 * @Date:13:05 2017/7/3
 * @Email:tao8.wang@changhong.com
 */
@Service
public class ExecutionListenerService {

    //注入为我们自动配置好的服务
    @Autowired
    private RuntimeService runtimeService;

    public void startProcess(String key) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("processId", "executionListenerProcess");
        runtimeService.startProcessInstanceByKey(key, variables);
    }

    public void aaa() {
        System.out.println("ExecutionListenerService.aaa()");
    }

    public void bbb() {
        System.out.println("ExecutionListenerService.bbb()");
    }

}
