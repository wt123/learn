package com.wt.activiti.service;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author: wangtao
 * @Date:9:09 2017/7/4
 * @Email:tao8.wang@changhong.com
 */
@Service
public class SubProcessService {
    //注入为我们自动配置好的服务
    @Autowired
    private RuntimeService runtimeService;

    public void startProcess(String key) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("processId", key);
        variables.put("keyInMainProcess", "valueInMainProcess");
        variables.put("keyInMainProcess2", "valueInMainProcess2");
        runtimeService.startProcessInstanceByKey(key, variables);
    }

    public void aaa(DelegateExecution execution) {
        System.out.println("SubProcessService.aaa()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables=" + variables);
        System.out.println("---------------------------------");
    }

    public void bbb(DelegateExecution execution) {
        System.out.println("SubProcessService.bbb()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables=" + variables);
        System.out.println("---------------------------------");
    }

    public void child(DelegateExecution execution) {
        System.out.println("SubProcessService.child()2:change_valueInMainProcess");
        execution.setVariable("keyInSubProcess","valueInMainProcess_changed");
        execution.setVariable("keyInSubProcess2","valueInMainProcess2_changed");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables=" + variables);
        System.out.println("---------------------------------");
    }

    public void aaa2(DelegateExecution execution) {
        System.out.println("SubProcessService.aaa2()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables=" + variables);
        System.out.println("---------------------------------");
    }

    public void bbb2(DelegateExecution execution) {
        System.out.println("SubProcessService.bbb2()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables=" + variables);
        System.out.println("---------------------------------");
    }

    public void child2(DelegateExecution execution) {
        System.out.println("SubProcessService.child2()");
        Map<String, Object> variables = execution.getVariables();
        System.out.println("variables=" + variables);
        System.out.println("---------------------------------");
    }

}
