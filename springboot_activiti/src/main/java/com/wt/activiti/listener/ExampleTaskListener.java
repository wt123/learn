package com.wt.activiti.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.TaskListener;

import java.util.Map;

/**
 * @Description
 * @Author: wangtao
 * @Date:13:19 2017/7/3
 * @Email:tao8.wang@changhong.com
 */
public class ExampleTaskListener implements TaskListener {

    private Expression myVar;

    @Override
    public void notify(DelegateTask delegateTask) {
        delegateTask.setAssignee("wtt");
        System.out.println("ExampleTaskListener.notify():"+delegateTask.getEventName());
        delegateTask.setVariable("myVar", myVar.getValue(delegateTask).toString());
        Map<String, Object> variables = delegateTask.getVariables();
        System.out.println(variables);
    }
}
