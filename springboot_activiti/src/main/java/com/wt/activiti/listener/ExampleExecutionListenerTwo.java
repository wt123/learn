package com.wt.activiti.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.delegate.Expression;

/**
 * @Description
 * @Author: wangtao
 * @Date:13:19 2017/7/3
 * @Email:tao8.wang@changhong.com
 */
public class ExampleExecutionListenerTwo implements ExecutionListener {

    private Expression myVar;

    @Override
    public void notify(DelegateExecution execution) {
        System.out.println("ExampleExecutionListenerTwo.notify()");
        System.out.println("getExpressionText:"+myVar.getExpressionText());
        execution.setVariable("myVar",myVar.getValue(execution).toString());
        System.out.println(execution.getVariables());
    }
}
