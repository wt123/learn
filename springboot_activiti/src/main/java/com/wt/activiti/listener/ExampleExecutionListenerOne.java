package com.wt.activiti.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

/**
 * @Description
 * @Author: wangtao
 * @Date:13:19 2017/7/3
 * @Email:tao8.wang@changhong.com
 */
public class ExampleExecutionListenerOne implements ExecutionListener {
    @Override
    public void notify(DelegateExecution execution) {
        System.out.println("ExampleExecutionListenerOne.notify()");
        execution.setVariable("variableSetInExecutionListener", "firstValue");
        execution.setVariable("eventReceived", execution.getEventName());
    }
}
