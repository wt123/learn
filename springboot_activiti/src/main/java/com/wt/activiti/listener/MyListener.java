package com.wt.activiti.listener;

import org.springframework.stereotype.Service;

/**
 * @Description
 * @Author: wangtao
 * @Date:13:22 2017/7/3
 * @Email:tao8.wang@changhong.com
 */
@Service
public class MyListener {

    public void myTaskStart(String eventName) {
        System.out.println("MyListener.myTaskStart():" + eventName);
    }

    public void myTaskEnd(String eventName) {
        System.out.println("MyListener.myTaskEnd():" + eventName);
    }

}
