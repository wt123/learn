package com.wt.activiti.controller;

import com.wt.activiti.response.TaskResoponse;
import com.wt.activiti.service.SimpleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description
 * @Author: wangtao
 * @Date:8:37 2017/6/30
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("/simple")
public class SimpleController {

    @Autowired
    private SimpleService simpleService;

    @RequestMapping("/start")
    public String startProcess() {
        simpleService.startProcess("simpleProcess");
        return "ok";
    }

    @RequestMapping("/tasks/{assignee}")
    public List<TaskResoponse> getTasks(@PathVariable String assignee) {
        List<TaskResoponse> tasks = simpleService.getTasks(assignee);
        return tasks;
    }

    @RequestMapping("/complete/{taskId}")
    public String complete(@PathVariable String taskId) {
        simpleService.complete(taskId);
        return "ok";
    }

    @RequestMapping("/all")
    public String all() {
        System.out.println("----------------start process <simpleProcess>----------------");
        simpleService.startProcess("simpleProcess");
        System.out.println("----------------get user <wt> tasks----------------");
        List<TaskResoponse> tasks = simpleService.getTasks("wt");
        System.out.println("tasks:" + tasks.get(0));
        System.out.println("----------------complete tasks----------------");
        simpleService.complete(tasks.get(0).getId());
        System.out.println("----------------process over----------------");
        return "ok";
    }

}
