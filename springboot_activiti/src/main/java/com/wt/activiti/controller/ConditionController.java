package com.wt.activiti.controller;


import com.wt.activiti.response.TaskResoponse;
import com.wt.activiti.service.ConditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description 条件顺序流，判定条件为true的分支都执行，并行执行，判定条件里面的值可以用variable，也可以是表达式
 * @Author: wangtao
 * @Date:10:01 2017/6/30
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("/condition")
public class ConditionController {

    @Autowired
    private ConditionService conditionService;

    @RequestMapping("/start/{aaa}")
    public String startProcess(@PathVariable boolean aaa) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("processId", "conditionProcess");
        variables.put("variable_aaa", aaa);
        conditionService.startProcess("conditionProcess", variables);
        return "ok";
    }

}
