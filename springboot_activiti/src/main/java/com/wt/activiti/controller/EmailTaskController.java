package com.wt.activiti.controller;

import com.wt.activiti.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author: wangtao
 * @Date:9:07 2017/7/3
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("/emailTask")
public class EmailTaskController {

    @Autowired
    private EmailService emailService;

    @RequestMapping("/start")
    public String start() {
        emailService.startProcess("eamilTaskProcess");
        return "ok";
    }

}
