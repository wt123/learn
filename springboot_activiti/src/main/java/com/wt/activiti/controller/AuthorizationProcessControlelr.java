package com.wt.activiti.controller;

import com.wt.activiti.service.AuthorizationProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description 认证流程启动还存在问题
 * @Author: wangtao
 * @Date:13:33 2017/7/6
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("/authProcess")
public class AuthorizationProcessControlelr {

    @Autowired
    private AuthorizationProcessService authService;

    @RequestMapping("/start/{userId}")
    public String startProcess(@PathVariable String userId) {
        authService.startProcess("authorizationProcess", userId);
        return "ok";
    }

    @RequestMapping("/query1/{userId}")
    public String query1(@PathVariable String userId) {
        authService.queryProcessDefinition(userId);
        return "ok";
    }

    @RequestMapping("/query2/{processId}")
    public String query2(@PathVariable String processId) {
        authService.queryIdentityLink(processId);
        return "ok";
    }

    @RequestMapping("/query3/{processId}")
    public String queryAuthProcessUser(@PathVariable String processId) {
        authService.queryAuthProcessUser(processId);
        return "ok";
    }

}
