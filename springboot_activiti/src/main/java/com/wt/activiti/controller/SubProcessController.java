package com.wt.activiti.controller;

import com.wt.activiti.service.SubProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author: wangtao
 * @Date:9:09 2017/7/4
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("/subProcess")
public class SubProcessController {

    @Autowired
    private SubProcessService subProcessService;

    @RequestMapping("/start")
    public String startProcess() {
        subProcessService.startProcess("subProcess");
        return "ok";
    }

    //使用subProcess标签的子流程会共享主流程variables
    @RequestMapping("/start2")
    public String startProcess2() {
        subProcessService.startProcess("subProcess2");
        return "ok";
    }
}
