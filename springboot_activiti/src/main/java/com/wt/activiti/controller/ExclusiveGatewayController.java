package com.wt.activiti.controller;

import com.wt.activiti.service.ExclusiveGatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description 默认顺序流，当其他分支都为false的时候，执行默认分支，否则执行第一个条件为true的分支
 * 该默认分支必须能够执行下一个task，即默认分支在到达第一个task的flow不允许使用条件流，
 * 排他分支只需要去掉<exclusiveGateway>的default属性
 * 排他分支如果没有分支被选中，则抛出异常
 * @Author: wangtao
 * @Date:10:58 2017/6/30
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("/exclusiveGateway")
public class ExclusiveGatewayController {

    @Autowired
    private ExclusiveGatewayService exclusiveGatewayService;

    @RequestMapping("/start/{branch}")
    public String startProcess(@PathVariable int branch) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("processId", "exclusiveGatewayProcess");
        variables.put("branch", branch);
        exclusiveGatewayService.startProcess("exclusiveGatewayProcess", variables);
        return "ok";
    }

    @RequestMapping("/startNoDefault/{branch}")
    public String startProcessNoDefault(@PathVariable int branch) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("processId", "exclusiveGatewayProcessNoDefault");
        variables.put("branch", branch);
        exclusiveGatewayService.startProcess("exclusiveGatewayProcessNoDefault", variables);
        return "ok";
    }

}
