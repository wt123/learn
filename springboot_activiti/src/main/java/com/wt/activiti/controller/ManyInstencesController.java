package com.wt.activiti.controller;

import com.wt.activiti.service.ManyInstencesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description 多实例执行的时候，如果有两个task都是多实例的，比如第一个有2个实例，第二个是3个实例，执行的时候会把第一个task的2个实例执行完，然后再去执行第二个task的3个实例，
 *              注意：并不是第一个task的两个实例都会去产生第二个task的3个实例来执行
 * @Author: wangtao
 * @Date:16:38 2017/7/3
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("/manyInstences")
public class ManyInstencesController {

    @Autowired
    private ManyInstencesService manyInstencesService;

    @RequestMapping("/start")
    public String startProcess() {
        manyInstencesService.startProcess("manyInstencesProcess");
        return "ok";
    }
}
