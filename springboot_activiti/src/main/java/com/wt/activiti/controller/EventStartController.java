package com.wt.activiti.controller;

import com.wt.activiti.service.EventStartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author: wangtao
 * @Date:8:25 2017/7/6
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("/event")
public class EventStartController {

    //注入为我们自动配置好的服务
    @Autowired
    private EventStartService eventStartService;

    /**
     * 消息启动事件，启动流程时使用的时message标签的name属性，而不是id
     * xml中定义的时候才使用的id
     * 信号启动事件和消息启动事件感觉上一致，没有做示例
     *
     * @return
     */
    @RequestMapping("/messageStart")
    public String messageStartProcess() {
        eventStartService.startProcess("myMsgName");
        return "ok";
    }

    /**
     * 异常启动事件只能用在子流程上，异常启动事件不能用于启动流程实例
     * 子流程上使用异常启动事件时必须设置属性triggeredByEvent=true，
     * 抛出的异常信息必须能和xml重定义的errorCode的值一致，类型为string
     *
     * @return
     */
    @RequestMapping("/errorStart")
    public String errorStartProcess() {
        eventStartService.startProcessByKey("errorProcess");
        return "ok";
    }


    /**
     * 异常终止事件和异常边界事件
     *
     * @return
     */
    @RequestMapping("/errAndBoundary")
    public String errorEndAndBoundaryStartProcess() {
        eventStartService.startProcessByKey("errorEndAndBoundaryEventProcess");
        return "ok";
    }


}
