package com.wt.activiti.controller;

import com.wt.activiti.service.InclusiveGatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description 包容分支，是排他分支和并行分支的结合，在并行的时候可以加条件，只有条件成立的顺序流才会被创建，没有条件结果为true时抛出异常
 *              可以指定默认的输出顺序流
 * @Author: wangtao
 * @Date:15:32 2017/6/30
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("/inclusiveGateway")
public class InclusiveGatewayController {

    @Autowired
    private InclusiveGatewayService inclusiveGatewayService;

    @RequestMapping("/start/{branch}")
    public String startProcess(@PathVariable int branch) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("processId", "inclusiveGatewayProcess");
        variables.put("branch", branch);
        inclusiveGatewayService.startProcess("inclusiveGatewayProcess", variables);
        return "ok";
    }
}
