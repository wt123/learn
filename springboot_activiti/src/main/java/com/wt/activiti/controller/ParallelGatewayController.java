package com.wt.activiti.controller;

import com.wt.activiti.service.ParallelGatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description 并行分支，不会计算条件，flow上的条件会被忽略掉，在分支合并的时候，需要等待当前需要合并的所有分支全部执行完，才执行下一步
 *              看控制台输出：示例中开始执行会先完成分支servicetask1（执行方法aaa()）和servicetask2(执行方法bbb())，
 *              接下来等待userTask(task所有人'wt',可以通过SimpleController里面的查询任务来查询)完成,然后执行serviceTask3(执行方法ccc()),
 *              合并，执行servicetask4(执行方法ddd()).
 * @Author: wangtao
 * @Date:14:34 2017/6/30
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("/parallelGateway")
public class ParallelGatewayController {

    @Autowired
    private ParallelGatewayService parallelGatewayService;

    @RequestMapping("/start/{branch}")
    public String startProcess(@PathVariable int branch) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("processId", "parallelGatewayProcess");
        variables.put("branch", branch);
        parallelGatewayService.startProcess("parallelGatewayProcess", variables);
        return "ok";
    }

}
