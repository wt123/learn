package com.wt.activiti.controller;

import com.alibaba.fastjson.JSONObject;
import com.wt.activiti.entity.VacationEntity;
import com.wt.activiti.response.TaskResoponse;
import com.wt.activiti.service.VacationDemoService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author: wangtao
 * @Date:10:50 2017/7/7
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("/vacation")
public class VacationDemoController {

    @Autowired
    private VacationDemoService vdService;

    @Autowired
    private ProcessEngine processEngine;

    @RequestMapping("/start")
    public String startProcess(@RequestBody JSONObject params) {
        String userId = params.getString("userId");
        String days = params.getString("days");
        String reason = params.getString("reason");
        VacationEntity.veMap.put(userId, new VacationEntity(userId, days, reason));
        Map<String, Object> variables = new HashMap<>();
        variables.put("user", new VacationEntity(userId, days, reason));
        vdService.startProcess("vacationDemoProcess", variables);
        return "ok";
    }

    @RequestMapping("/tasks/{assignee}")
    public List<TaskResoponse> getTasks(@PathVariable String assignee) {
        List<TaskResoponse> tasks = vdService.getTasks(assignee);
        return tasks;
    }

    @RequestMapping("/complete/{taskId}/{flag}")
    public Map<String, Object> complete(@PathVariable String taskId, @PathVariable boolean flag) {
        return vdService.completeTasks(flag, taskId);
    }

    @RequestMapping("/claim/{taskId}/{userId}")
    public String claim(@PathVariable String taskId, @PathVariable String userId) {
        vdService.claim(taskId, userId);
        return "ok";
    }

    @RequestMapping("/hd")
    public String hd() {
        vdService.queryHistory();
        return "ok";
    }

    @RequestMapping("test")
    public void en() {
        ProcessEngineConfiguration p = processEngine.getProcessEngineConfiguration();
        System.out.println(p.getActivityFontName());
        vdService.startProcess("vacationDemoProcess2");
    }

}
