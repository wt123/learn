package com.wt.activiti.controller;

import com.wt.activiti.service.ExecutionListenerService;
import com.wt.activiti.service.TaskListenerService;
import com.wt.activiti.service.TimerTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author: wangtao
 * @Date:13:03 2017/7/3
 * @Email:tao8.wang@changhong.com
 */
@RestController
@RequestMapping("listener")
public class ListenerController {

    @Autowired
    private ExecutionListenerService executionListenerService;


    @Autowired
    private TaskListenerService taskListenerService;

    @RequestMapping("/executionStart")
    public String startProcess() {
        executionListenerService.startProcess("executionListenerProcess");
        return "ok";
    }

    @RequestMapping("/taskStart")
    public String startProcess2() {
        taskListenerService.startProcess("taskListenerProcess");
        return "ok";
    }

}
