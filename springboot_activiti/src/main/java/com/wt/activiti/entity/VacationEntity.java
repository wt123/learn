package com.wt.activiti.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author: wangtao
 * @Date:11:00 2017/7/7
 * @Email:tao8.wang@changhong.com
 */
public class VacationEntity implements Serializable {

    public static Map<String, VacationEntity> veMap = new HashMap<>();

    private String userId;
    private String days;
    private String reason;

    public VacationEntity() {
    }

    public VacationEntity(String userId, String days, String reason) {
        this.userId = userId;
        this.days = days;
        this.reason = reason;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "VacationEntity{" +
                "userId='" + userId + '\'' +
                ", days='" + days + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
