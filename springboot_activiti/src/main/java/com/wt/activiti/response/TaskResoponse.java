package com.wt.activiti.response;

/**
 * @Description
 * @Author: wangtao
 * @Date:9:14 2017/6/30
 * @Email:tao8.wang@changhong.com
 */
public class TaskResoponse {
    private String id;
    private String name;

    public TaskResoponse(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}' + ',';
    }
}
