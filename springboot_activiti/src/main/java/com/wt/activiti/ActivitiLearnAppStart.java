package com.wt.activiti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description
 * @Author: wangtao
 * @Date:8:47 2017/6/30
 * @Email:tao8.wang@changhong.com
 */
@SpringBootApplication
public class ActivitiLearnAppStart {
    public static void main(String[] args) {
        SpringApplication.run(ActivitiLearnAppStart.class, args);
    }
}
