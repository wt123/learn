package com.wt.hotdeploy;

/**
 * @description
 * @author: wangtao
 * @date:10:45 2019/10/24
 * @email:taow02@jumei.com
 */
public class NewBean implements BaseBean {
	@Override
	public String toString() {
		System.out.println("this is new bean; classLoader = " + getClass().getClassLoader());
		return "";
	}
}
