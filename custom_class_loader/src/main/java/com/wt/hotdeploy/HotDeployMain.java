package com.wt.hotdeploy;

/**
 * @description
 * @author: wangtao
 * @date:15:56 2019/6/14
 * @email:taow02@jumei.com
 */
public class HotDeployMain {
	public static void main(String[] args) {
		MyService myService = new MyService();
		//默认初始化一个，这个oldbean由appClassloader加载
		myService.baseBean = new OldBean();
		try {
			CustomClassLoader loader = new CustomClassLoader();
			int count = 0;
			boolean isOldBean = true;
			Class<?> clz;
			while (true) {
				myService.print();
				Thread.sleep(1000);
				if (++count % 3 == 0) {
					// 每三次就切换一次bean，这个时候我们使用自定义的classloader去加载，由于双亲委托机制，所以实际上还是由AppClassloader加载的，
					// 但是我们可以使用此种方式修改service中的bean定义，实现热加载，这种方式是应用入侵型的，因为我们必须要在应用代码中实现这种替换
					// 可以使用java agent，获取Instrumentation对象的方式来进行无入侵的热替换
					// agent 热替换原理 https://blog.csdn.net/qq_41701956/article/details/84929729
					if (isOldBean) {
						clz = loader.loadClass("com.wt.hotdeploy.NewBean");
					} else {
						clz = loader.loadClass("com.wt.hotdeploy.OldBean");
					}
					isOldBean = !isOldBean;
					myService.baseBean = (BaseBean) clz.newInstance();
				}
			}
		} catch (Throwable e) {
			System.out.println("load UsrInterface fail   " + e.getMessage());
		}
	}
}
