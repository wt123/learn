package com.wt.demo;

import com.wt.test.UserInterface;

/**
 * @description
 * @author: wangtao
 * @date:18:09 2019/6/14
 * @email:taow02@jumei.com
 */
public class User implements UserInterface {
	public User() {
	}

	@Override
	public String hello(String a) {
		return "hello " + a;
	}
}
