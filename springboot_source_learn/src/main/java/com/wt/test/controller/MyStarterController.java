package com.wt.test.controller;

import com.wt.test.service.MyStarterService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description
 * @author: wangtao
 * @date:15:20 2019/7/30
 * @email:386427665@qq.com
 */
@RestController
@RequestMapping("/starter")
public class MyStarterController {

    @Resource
    private MyStarterService myStarterService;

    @RequestMapping("/test")
    public Object starter() {
        myStarterService.helloStarter("wt", System.currentTimeMillis());
        return "success";
    }

}
