package com.wt.test.controller;

//import com.wt.myaop.service.MyAopService;
import com.wt.test.entity.UserEntity;
import com.wt.test.service.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description
 * @author: wangtao
 * @date:20:27 2019/6/3
 * @email:386427665@qq.com
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private UserEntityService userEntityService;

    @RequestMapping("/test")
    public String test(){
        return "test success";
    }

    @RequestMapping("/save")
    public Object save(){
        userEntityService.save(new UserEntity("wt","25","nan"));
        return userEntityService.getUsers();
    }

}
