package com.wt.test.mapper;

import com.wt.test.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * @description
 * @author: wangtao
 * @date:17:24 2019/5/28
 * @email:taow02@jumei.com
 */
@Mapper
public interface UserEntityMapper {

	void insertUser(UserEntity user);

	List<UserEntity> getUser();

}
