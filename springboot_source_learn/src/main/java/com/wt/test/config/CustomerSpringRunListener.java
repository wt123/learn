package com.wt.test.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @description
 * @author: wangtao
 * @date:17:51 2019/7/25
 * @email:taow02@jumei.com
 */
public class CustomerSpringRunListener implements SpringApplicationRunListener {

	public CustomerSpringRunListener(SpringApplication application, String[] args) {
		System.out.println("CustomerSpringRunListener(SpringApplication, String[])");
	}

	@Override
	public void starting() {
		System.out.println("-----------------------CustomerSpringRunListener.starting()----------------------");
	}

	@Override
	public void environmentPrepared(ConfigurableEnvironment environment) {

	}

	@Override
	public void contextPrepared(ConfigurableApplicationContext context) {

	}

	@Override
	public void contextLoaded(ConfigurableApplicationContext context) {

	}

	@Override
	public void started(ConfigurableApplicationContext context) {

	}

	@Override
	public void running(ConfigurableApplicationContext context) {

	}

	@Override
	public void failed(ConfigurableApplicationContext context, Throwable exception) {

	}
}
