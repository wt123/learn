package com.wt.test.config;

import org.springframework.beans.BeansException;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @description
 * @author: wangtao
 * @date:10:15 2019/7/26
 * @email:taow02@jumei.com
 */
@Component
public class CustomerApplicationRunner implements ApplicationRunner, ApplicationContextAware {
	@Override
	public void run(ApplicationArguments args) throws Exception {
		System.out.println("-----------CustomerApplicationRunner running-----------启动参数args = " + Arrays
				.toString(args.getSourceArgs()));
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("-----"+applicationContext);
	}
}
