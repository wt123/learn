package com.wt.test.config;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @description
 * @author: wangtao
 * @date:11:33 2019/6/4
 * @email:386427665@qq.com
 */
public class MyInitializer implements ApplicationContextInitializer {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        System.out.println("------------------this is MyInitializer-------------------");
    }
}
