package com.wt.test.config;

import org.springframework.beans.BeansException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @description
 * @author: wangtao
 * @date:10:16 2019/7/26
 * @email:taow02@jumei.com
 */
@Component
public class CustomerCommandLineRunner implements CommandLineRunner, ApplicationContextAware {
	@Override
	public void run(String... args) throws Exception {
		System.out.println("---------------CustomerCommandLineRunner running------------------启动参数args = " + Arrays
				.toString(args));
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("-----"+applicationContext);
	}
}
