package com.wt.test.service;

/**
 * @description
 * @author: wangtao
 * @date:15:15 2019/7/30
 * @email:386427665@qq.com
 */
public interface MyStarterService {

    void helloStarter(String msg, Long currentTime);

}
