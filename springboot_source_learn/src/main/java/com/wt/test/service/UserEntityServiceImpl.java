package com.wt.test.service;

import com.wt.test.entity.UserEntity;
import com.wt.test.mapper.UserEntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description
 * @author: wangtao
 * @date:10:55 2019/6/4
 * @email:386427665@qq.com
 */
@Service
public class UserEntityServiceImpl implements UserEntityService {

    @Autowired
    private UserEntityMapper userEntityMapper;

    @Override
    public void save(UserEntity user) {
        userEntityMapper.insertUser(user);
    }

    @Override
    public List<UserEntity> getUsers() {
        return userEntityMapper.getUser();
    }
}
