package com.wt.test.service;

import com.wt.myaop.anno.MyAnnotation;
import org.springframework.stereotype.Service;

/**
 * @description
 * @author: wangtao
 * @date:15:15 2019/7/30
 * @email:386427665@qq.com
 */
@Service
public class MyStarterServiceImpl implements MyStarterService {

    @Override
    @MyAnnotation
    public void helloStarter(String msg, Long currentTime) {
        System.out.println("----hello starter,msg = " + msg + ",currentTime = " + currentTime);
    }

}
