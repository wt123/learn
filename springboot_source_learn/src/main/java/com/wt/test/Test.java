package com.wt.test;

import org.springframework.boot.SpringApplication;

import java.io.IOException;
import java.io.InputStream;

/**
 * @description
 * @author: wangtao
 * @date:10:46 2019/6/5
 * @email:386427665@qq.com
 */
public class Test {
    public static void main(String[] args) throws Exception {

        ClassLoader clazzLoader = new ClassLoader() {
            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                try {
                    String clazzName = name.substring(name.lastIndexOf(".") + 1) + ".class";

                    InputStream is = getClass().getResourceAsStream(clazzName);
                    if (is == null) {
                        return super.loadClass(name);
                    }
                    byte[] b = new byte[is.available()];
                    is.read(b);
                    return defineClass(name, b, 0, b.length);
                } catch (IOException e) {
                    throw new ClassNotFoundException(name);
                }
            }
        };

        String currentClass = "com.wt.test.BootStartMain";
        Class<?> clazz = clazzLoader.loadClass(currentClass);
        Object obj = clazz.newInstance();

        System.out.println(obj.getClass());
        System.out.println(obj instanceof BootStartMain);

        System.out.println(new SpringApplication().getClassLoader());
    }
}
