package com.wt.demo.service;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

/**
 * @description
 * @author: wangtao
 * @date:15:12 2019/6/21
 * @email:taow02@jumei.com
 */
@Service
public class TestHandler {

	public Mono<ServerResponse> hello(ServerRequest request){
		return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN)
				.body(BodyInserters.fromObject("Hello, wt from Router&handler!"));
	}

	/**
	 * 每秒返回一次时间，server->client推送
	 * @param request
	 * @return
	 */
	public Mono<ServerResponse> times(ServerRequest request){
		return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM)
				.body(Flux.interval(Duration.ofSeconds(1)).map(l->new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())),String.class);
	}


}
