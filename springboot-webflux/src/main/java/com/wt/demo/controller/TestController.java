package com.wt.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

/**
 * @description
 * @author: wangtao
 * @date:15:00 2019/6/21
 * @email:taow02@jumei.com
 */
@RestController
@RequestMapping("/test")
public class TestController {

	@RequestMapping("/hello")
	public Mono<String> hello(){
		return Mono.just("hello wt from controller");
	}

	/**
	 * controller形式，每秒返回一次时间，server->client推送
	 * @return
	 */
	@RequestMapping("/times")
	public Flux<String> time(){
		return Flux.interval(Duration.ofSeconds(1)).map(v->new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	}

}
