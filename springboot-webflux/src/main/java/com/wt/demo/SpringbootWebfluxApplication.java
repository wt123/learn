package com.wt.demo;

import com.wt.demo.service.TestHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * @description
 * @author: wangtao
 * @date:14:41 2019/6/21
 * @email:taow02@jumei.com
 */
@SpringBootApplication
public class SpringbootWebfluxApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootWebfluxApplication.class, args);
	}

	@Bean
	public RouterFunction<ServerResponse> routeCity(TestHandler testHandler) {
		return RouterFunctions
				.route(RequestPredicates.GET("/hello").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)), testHandler::hello)
				.andRoute(RequestPredicates.GET("/times"), testHandler::times);
	}

}
