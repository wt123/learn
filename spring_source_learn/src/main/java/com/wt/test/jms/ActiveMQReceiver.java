package com.wt.test.jms;

import org.apache.activemq.spring.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * @description
 * @author: wangtao
 * @date:9:55 2019/6/3
 * @email:386427665@qq.com
 */
public class ActiveMQReceiver {
    public static void main(String[] args) throws Exception {
        ActiveMQConnectionFactory fac = new ActiveMQConnectionFactory();
        Connection conn = fac.createConnection();
        Session session = conn.createSession(true, Session.AUTO_ACKNOWLEDGE);
        Destination destination = session.createQueue("my-queue");
        MessageConsumer consumer = session.createConsumer(destination);
        for (int i = 0; i < 3; i++) {
            TextMessage message = (TextMessage) consumer.receive();
            session.commit();
            System.out.println("收到消息："+message.getText());
        }
        session.close();
        conn.close();
    }
}
