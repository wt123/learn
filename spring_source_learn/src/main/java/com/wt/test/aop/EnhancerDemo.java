package com.wt.test.aop;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @description
 * @author: wangtao
 * @date:10:43 2019/5/28
 * @email:taow02@jumei.com
 */
public class EnhancerDemo {
	public static void main(String[] args) {
		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(EnhancerDemo.class);
		enhancer.setCallback(new MethodInterceptorImpl());

		EnhancerDemo o = (EnhancerDemo) enhancer.create();
		o.test();
		System.out.println(o);
	}

	public void test(){
		System.out.println("EnhancerDemo.test()");
	}

	private static class MethodInterceptorImpl implements MethodInterceptor {

		@Override
		public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
			System.out.println("before "+method);
			Object result = methodProxy.invokeSuper(o, args);
			System.out.println("after "+methodProxy);
			return result;
		}
	}
}
