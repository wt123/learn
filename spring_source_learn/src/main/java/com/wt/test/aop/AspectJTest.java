package com.wt.test.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * @description
 * @author: wangtao
 * @date:11:50 2019/5/27
 * @email:taow02@jumei.com
 */
@Aspect
public class AspectJTest {

	@Pointcut("execution(* *.test(..))")
	public void test(){}

	@Before(value = "test()")
	public void before(){
		System.out.println("before------------");
	}
	@After(value = "test()")
	public void after(){
		System.out.println("after-------------");
	}
	@Around(value = "test()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		System.out.println("around before-------------");
		Object o = point.proceed();
		System.out.println("around after--------------");
		return o;
	}

}
