package com.wt.test.aop;

/**
 * @description
 * @author: wangtao
 * @date:18:23 2019/5/27
 * @email:taow02@jumei.com
 */
public interface ITestBean {
	void test();
}
