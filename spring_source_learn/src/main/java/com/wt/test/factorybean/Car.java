package com.wt.test.factorybean;

/**
 * @description
 * @author: wangtao
 * @date:17:42 2019/5/21
 * @email:taow02@jumei.com
 */
public class Car {
	private String brand;
	private int price;

	public String getBrand() {
		return brand;
	}

	public Car setBrand(String brand) {
		this.brand = brand;
		return this;
	}

	public int getPrice() {
		return price;
	}

	public Car setPrice(int price) {
		this.price = price;
		return this;
	}

	@Override
	public String toString() {
		return "Car{" + "brand='" + brand + '\'' + ", price=" + price + '}';
	}
}
