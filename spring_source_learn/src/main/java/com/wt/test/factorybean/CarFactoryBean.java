package com.wt.test.factorybean;

import org.springframework.beans.factory.FactoryBean;

/**
 * @description
 * @author: wangtao
 * @date:17:42 2019/5/21
 * @email:taow02@jumei.com
 */
public class CarFactoryBean implements FactoryBean<Car> {
	private String carInfo;
	@Override
	public Car getObject() throws Exception {
		String[] cs = carInfo.split(",");
		return new Car().setBrand(cs[0]).setPrice(Integer.parseInt(cs[1]));
	}

	@Override
	public Class<?> getObjectType() {
		return Car.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public String getCarInfo() {
		return carInfo;
	}

	public CarFactoryBean setCarInfo(String carInfo) {
		this.carInfo = carInfo;
		return this;
	}
}
