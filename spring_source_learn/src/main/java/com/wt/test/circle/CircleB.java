package com.wt.test.circle;

/**
 * @description
 * @author: wangtao
 * @date:16:40 2019/5/23
 * @email:taow02@jumei.com
 */
public class CircleB {

	private CircleA circleA;

	public CircleA getCircleA() {
		return circleA;
	}

	public CircleB setCircleA(CircleA circleA) {
		this.circleA = circleA;
		return this;
	}
}
