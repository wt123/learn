package com.wt.test.circle;

import org.springframework.beans.factory.DisposableBean;

/**
 * @description
 * @author: wangtao
 * @date:16:40 2019/5/23
 * @email:taow02@jumei.com
 */
public class CircleA implements DisposableBean {

	private CircleB circleB;

	public CircleB getCircleB() {
		return circleB;
	}

	public CircleA setCircleB(CircleB circleB) {
		this.circleB = circleB;
		return this;
	}

	@Override
	public void destroy() throws Exception {

	}
}
