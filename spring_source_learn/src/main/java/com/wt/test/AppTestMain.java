package com.wt.test;

import com.wt.test.aop.ITestBean;
import com.wt.test.aop.TestBean;
import com.wt.test.circle.CircleA;
import com.wt.test.circle.CircleB;
import com.wt.test.customtag.User;
import com.wt.test.factorybean.Car;
import com.wt.test.factorybean.CarFactoryBean;
import com.wt.test.jdbc.UserEntity;
import com.wt.test.jdbc.UserService;
import com.wt.test.lookup.AbstractAnimalService;
import com.wt.test.mybatis.UserEntityMapper;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 配置文件xml的解析的时候，将ClassPathXmlApplicationContext换成XmlBeanFactory会更容易理解一些
 * 因为他们都是用的X没了BeanDefinitionLoader进行解析的，我这里全部都是用的ClassPathXmlApplicationContext
 *
 * @author: wangtao
 * @date:11:30 2019/5/21
 * @email:taow02@jumei.com
 */
public class AppTestMain {

	public static void main(String[] args) throws Exception {
//						customtag();
						factoryBean();
		//				lookUp();
		//		circleSolution();
		//		aop();
		//		jdbc();
		//		mybatis();
//		jdbcTx();

	}

	private static void jdbcTx() {
		ClassPathXmlApplicationContext x = new ClassPathXmlApplicationContext("application-jdbc_tx.xml");
		UserService userService = (UserService) x.getBean("userService");
		try {
			userService.saveException(new UserEntity("jdbctx", "25", "man"));
		}catch (Exception e){
			e.printStackTrace();
		}
		System.out.println(userService.getUsers());
	}

	private static void mybatis() {
		ClassPathXmlApplicationContext x = new ClassPathXmlApplicationContext("application-mybatis.xml");
		UserEntityMapper userEntityMapper = x.getBean(UserEntityMapper.class);
		//		UserEntityMapper userEntityMapper = (UserEntityMapper) x.getBean("userEntityMapper");
		userEntityMapper.insertUser(new UserEntity("mybatis", "3", "boy"));
		System.out.println(userEntityMapper.getUser("mybatis"));
	}

	private static void jdbc() {
		ClassPathXmlApplicationContext x = new ClassPathXmlApplicationContext("application-jdbc.xml");
		UserService userService = (UserService) x.getBean("userService");
		userService.save(new UserEntity("wangtao", "25", "man"));
		System.out.println(userService.getUsers());
	}

	private static void aop() {
		ClassPathXmlApplicationContext x = new ClassPathXmlApplicationContext("application-aop.xml");
		//		ITestBean b= (ITestBean) x.getBean("testBean");
		TestBean b = (TestBean) x.getBean("testBean");
		b.test();
	}

	private static void circleSolution() {
		ClassPathXmlApplicationContext x = new ClassPathXmlApplicationContext("application-circle.xml");
		CircleA a = (CircleA) x.getBean("circleA");
		CircleB b = (CircleB) x.getBean("circleB");
		System.out.println(a);
		System.out.println(b);
		System.out.println(a.getCircleB() == b);
		System.out.println(b.getCircleA() == a);
	}

	private static void lookUp() {
		ClassPathXmlApplicationContext x = new ClassPathXmlApplicationContext("application-service.xml");
		AbstractAnimalService catService = (AbstractAnimalService) x.getBean("cat");
		AbstractAnimalService dogService = (AbstractAnimalService) x.getBean("dog");
		catService.getAnimal().getMe();
		dogService.getAnimal().getMe();
	}

	public static void factoryBean() throws Exception {
		ClassPathXmlApplicationContext x = new ClassPathXmlApplicationContext("application-bean.xml");
		Car car = (Car) x.getBean("car");
		System.out.println(car);
		CarFactoryBean carFac = (CarFactoryBean) x.getBean("&car");
		Car car2 = carFac.getObject();
		System.out.println(car2);
		System.out.println(car == car2);
	}

	public static void customtag() {
		ClassPathXmlApplicationContext x = new ClassPathXmlApplicationContext("application-customtag.xml");
		User user = (User) x.getBean("user");
		User user2 = (User) x.getBean("wt");
		System.out.println(user);
		System.out.println(user == user2);
	}


}
