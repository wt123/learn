package com.wt.test.jdbc;

import java.util.List;

/**
 * @description
 * @author: wangtao
 * @date:15:50 2019/5/28
 * @email:taow02@jumei.com
 */
public interface UserService {

	void save(UserEntity user);

	List<UserEntity> getUsers();

	void saveException(UserEntity user) throws Exception;
}
