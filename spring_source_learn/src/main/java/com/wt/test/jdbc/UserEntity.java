package com.wt.test.jdbc;

/**
 * @description
 * @author: wangtao
 * @date:16:02 2019/5/28
 * @email:taow02@jumei.com
 */
public class UserEntity {

	private String name;
	private String age;
	private String sex;

	public UserEntity() {
	}

	public UserEntity(String name, String age, String sex) {
		this.name = name;
		this.age = age;
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public UserEntity setName(String name) {
		this.name = name;
		return this;
	}

	public String getAge() {
		return age;
	}

	public UserEntity setAge(String age) {
		this.age = age;
		return this;
	}

	public String getSex() {
		return sex;
	}

	public UserEntity setSex(String sex) {
		this.sex = sex;
		return this;
	}

	@Override
	public String toString() {
		return "{" + "name='" + name + '\'' + ", age='" + age + '\'' + ", sex='" + sex + '\'' + '}';
	}
}
