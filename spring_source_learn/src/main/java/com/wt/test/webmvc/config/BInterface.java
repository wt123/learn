package com.wt.test.webmvc.config;

import java.util.Properties;

/**
 * @description
 * @author: wangtao
 * @date:11:32 2019/2/20
 * @email:taow02@jumei.com
 */
public interface BInterface {

	String say(String str);

	Properties getProp();

}
