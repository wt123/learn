package com.wt.test.webmvc.service;

import com.wt.test.webmvc.entity.Bar;
import com.wt.test.webmvc.entity.Foo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description
 * @author: wangtao
 * @date:17:40 2019/2/28
 * @email:taow02@jumei.com
 */
@Configuration
public class AppConfig {

	@Bean
	public Foo getFoo() {
		return new Foo(getBar());
	}

	@Bean(initMethod = "init")
	public Bar getBar() {
		return new Bar();
	}

}