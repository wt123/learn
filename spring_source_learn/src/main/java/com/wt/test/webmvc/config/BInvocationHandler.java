package com.wt.test.webmvc.config;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * @description
 * @author: wangtao
 * @date:11:33 2019/2/20
 * @email:taow02@jumei.com
 */
public class BInvocationHandler implements InvocationHandler {

	private Properties properties;

	public BInvocationHandler() {
	}

	public BInvocationHandler(Properties properties) {
		this.properties = properties;
	}

	public Properties getProperties() {
		return properties;
	}

	public BInvocationHandler setProperties(Properties properties) {
		this.properties = properties;
		return this;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("invoke " + method.getDeclaringClass().getName() + "." + method.getName() + "()");
		if (method.getDeclaringClass() == Object.class) {
			return method.invoke(this, args);
		}
		if (method.getName().contains("say")) {
			return "Hello " + args[0];
		} else if (method.getName().contains("getProp")) {
			return properties;
		}
		return method.invoke(this, args);
	}
}
