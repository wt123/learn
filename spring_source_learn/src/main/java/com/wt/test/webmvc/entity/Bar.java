package com.wt.test.webmvc.entity;

import javax.annotation.PostConstruct;

/**
 * @description
 * @author: wangtao
 * @date:17:41 2019/2/28
 * @email:taow02@jumei.com
 */
public class Bar {

	@PostConstruct
	public void aaa(){
		System.out.println("--------------Bar------------------@postConstruct");
	}

	public Bar() {
		System.out.println("--------------Bar------------------Constructor");
	}

	public void init(){
		System.out.println("--------------Bar------------------initMethod");
	}

	public static void main(String[] args) {
		System.out.println("abcdefg");
		System.out.println("ABCDEFG");
		System.out.println("中文测试");
	}
}
