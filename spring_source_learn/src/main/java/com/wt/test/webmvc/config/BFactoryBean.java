package com.wt.test.webmvc.config;

import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.Proxy;
import java.util.Properties;

/**
 * @description
 * @author: wangtao
 * @date:13:53 2018/11/7
 * @email:taow02@jumei.com
 */
public class BFactoryBean implements FactoryBean {

	private Class<?> interfaceClz;

	private Properties properties;

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public Class<?> getInterfaceClz() {
		return interfaceClz;
	}

	public BFactoryBean setInterfaceClz(Class<?> interfaceClz) {
		this.interfaceClz = interfaceClz;
		return this;
	}

	@Override
	public Object getObject() throws Exception {
		return Proxy.newProxyInstance(interfaceClz.getClassLoader(), new Class[]{interfaceClz},
				new BInvocationHandler(properties));
	}

	@Override
	public Class<?> getObjectType() {
		return null;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
