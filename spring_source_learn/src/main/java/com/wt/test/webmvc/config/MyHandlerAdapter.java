package com.wt.test.webmvc.config;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter;

/**
 * 加入springbean之后，3.2.x版本容器不会去使用默认的adapter了，会报错，
 * 如果要使用需要将默认的adapter也配置上
 *
 * @author: wangtao
 * @date:14:28 2019/4/16
 * @email:taow02@jumei.com
 */
//@Component
public class MyHandlerAdapter extends SimpleControllerHandlerAdapter {
	public MyHandlerAdapter() {
		System.out.println("-----------------------MyHandlerAdappter.class init---------------------------");
	}
}
