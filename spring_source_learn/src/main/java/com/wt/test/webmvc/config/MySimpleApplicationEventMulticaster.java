package com.wt.test.webmvc.config;

import org.springframework.context.event.SimpleApplicationEventMulticaster;

/**
 * @description
 * @author: wangtao
 * @date:9:57 2019/2/20
 * @email:taow02@jumei.com
 */
public class MySimpleApplicationEventMulticaster extends SimpleApplicationEventMulticaster {
}
