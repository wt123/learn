package com.wt.test.webmvc.config;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;

/**
 * @description
 * @author: wangtao
 * @date:16:21 2019/2/28
 * @email:taow02@jumei.com
 */
public class MyBeanNameGenerator  implements BeanNameGenerator {

	public String generateBeanName(BeanDefinition definition, BeanDefinitionRegistry registry) {
		String generatedBeanName = definition.getBeanClassName();
		System.out.println("-------------class <"+generatedBeanName+"> used MyBeanNameGenerator--------------");
		return BeanDefinitionReaderUtils.generateBeanName(definition, registry);
	}
}