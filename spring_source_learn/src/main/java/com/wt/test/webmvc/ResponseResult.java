package com.wt.test.webmvc;

/**
 * @description
 * @author: wangtao
 * @date:9:45 2019/7/3
 * @email:386427665@qq.com
 */
public class ResponseResult {

    private int code;

    private String msg;

    public ResponseResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
