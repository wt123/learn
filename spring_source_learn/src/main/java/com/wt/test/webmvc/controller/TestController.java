package com.wt.test.webmvc.controller;

import com.wt.test.jdbc.UserEntity;
import com.wt.test.webmvc.ResponseResult;
import com.wt.test.webmvc.anno.MyResponse;
import com.wt.test.webmvc.config.BInterface;
import com.wt.test.webmvc.config.MyInitAware;
import com.wt.test.webmvc.entity.Bar;
import com.wt.test.webmvc.entity.Foo;
import com.wt.test.webmvc.service.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@DependsOn({"myInitAware"})
public class TestController {

	@Autowired
	private Foo foo;

	@Autowired
	private Bar bar;

	@Autowired
	private Util util;

	@Resource(name = "b2")
	private BInterface b2;

	@Resource(name = "b1")
	private BInterface b1;

	@RequestMapping("/test")
	@MyResponse
	public ResponseResult test(UserEntity userEntity) {
		System.out.println("Bar -->" + bar.toString());
		System.out.println("Bar instance in Foo-->" + foo.getBar().toString());
		System.out.println(b2.getProp().get("propName") + ":" + b1.getProp().get("propName"));
		System.out.println(b2.say("b2") + ":" + b1.say("b1"));
		System.out.println("ApplicationContext-->"+Util.getApplicationContext());
		return new ResponseResult(0,"success");
	}

	@RequestMapping("/testValueHandler")
	@MyResponse
	public ResponseResult testValueHandler() {
		return new ResponseResult(0,"success");
	}



	@RequestMapping("/testjsp")
	public String testjsp() {
		return "../jsp/success.jsp";
	}

	@RequestMapping("/testhtml")
	public String testhtml() {
		return "success.html";
	}

}
