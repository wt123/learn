package com.wt.test.webmvc.config;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @description
 * @author: wangtao
 * @date:11:11 2019/1/23
 * @email:taow02@jumei.com
 */
public class MyContextInitializer implements ApplicationContextInitializer {
	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {
		System.out.println("-------MyContextInitializer.initialize ---"+applicationContext.getClass().getName());
	}

}
