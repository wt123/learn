package com.wt.test.webmvc.config;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 * @description
 * @author: wangtao
 * @date:20:36 2019/1/21
 * @email:taow02@jumei.com
 */
public class MyWebApplicationContext extends XmlWebApplicationContext {

	protected void customizeBeanFactory(DefaultListableBeanFactory beanFactory) {
		super.customizeBeanFactory(beanFactory);
	}

	@Override
	protected DefaultListableBeanFactory createBeanFactory() {
		System.out.println("----begin create BeanFactory-----MyWebApplicationContext.createBeanFactory()------");
		return super.createBeanFactory();
	}

	@Override
	protected void initBeanDefinitionReader(XmlBeanDefinitionReader beanDefinitionReader) {
		super.initBeanDefinitionReader(beanDefinitionReader);
	}
}
