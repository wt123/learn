package com.wt.test.webmvc.config;

import org.springframework.web.context.ConfigurableWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * @description
 * @author: wangtao
 * @date:20:42 2019/1/21
 * @email:taow02@jumei.com
 */
public class MyDispatcherServlet extends DispatcherServlet {

	@Override
	protected void postProcessWebApplicationContext(ConfigurableWebApplicationContext wac) {
		System.out.println("--------------------MyDispatcherServlet.postProcessWebApplicationContext(" + wac.getClass().getName() + ")");
	}

	@Override
	public Class<?> getContextClass() {
		return MyWebApplicationContext.class;
	}
}
