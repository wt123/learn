package com.wt.test.webmvc.config;

import com.wt.test.webmvc.entity.Bar;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.annotation.Annotation;

/**
 * @description
 * @author: wangtao
 * @date:10:25 2018/12/11
 * @email:taow02@jumei.com
 */

public class CustomBeanPostProcessor implements BeanPostProcessor {

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println(bean.getClass().getName() + ":postProcessBeforeInitialization");
		Annotation[] annotations = bean.getClass().getAnnotations();
		for (Annotation annotation : annotations) {
			System.out.println(annotation.getClass().getName());
		}
		System.out.println(beanName+"init finish");
		if(bean instanceof Bar){
			System.out.println("--------------Bar------------BeanPostProcessor");
		}
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println(bean.getClass().getName() + ":postProcessAfterInitialization");
		return bean;
	}
}
