package com.wt.test.webmvc.lifecycle;

import org.springframework.context.Lifecycle;
import org.springframework.context.SmartLifecycle;

/**
 * @description
 * @author: wangtao
 * @date:9:48 2019/7/9
 * @email:386427665@qq.com
 */
public class CustomLifeCycle implements SmartLifecycle {
    @Override
    public void start() {
        System.out.println("---------CustomLifeCycle.start()---------");
    }

    @Override
    public void stop() {

    }

    @Override
    public boolean isRunning() {
        return false;
    }
}
