package com.wt.test.webmvc.entity;

/**
 * @description
 * @author: wangtao
 * @date:17:41 2019/2/28
 * @email:taow02@jumei.com
 */
public class Foo {

	private Bar bar;

	public Foo() {
	}

	public Foo(Bar bar) {
		this.bar = bar;
	}

	public Bar getBar() {
		return bar;
	}

	public Foo setBar(Bar bar) {
		this.bar = bar;
		return this;
	}
}
