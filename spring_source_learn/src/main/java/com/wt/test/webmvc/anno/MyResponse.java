package com.wt.test.webmvc.anno;

import java.lang.annotation.*;

/**
 * @description
 * @author: wangtao
 * @date:18:27 2019/7/2
 * @email:386427665@qq.com
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyResponse {
}
