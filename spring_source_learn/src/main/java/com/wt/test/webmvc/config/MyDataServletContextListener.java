package com.wt.test.webmvc.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @description
 * @author: wangtao
 * @date:11:03 2019/5/30
 * @email:386427665@qq.com
 */
public class MyDataServletContextListener implements ServletContextListener {

    private ServletContext servletContext;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        this.servletContext = sce.getServletContext();
        //servletcontext是全局的，应用启动之后可以通过servlet或jsp页面山获取这个值
        //getServeletcontext().getAttibute("mydata");或者${mydata}，看success.jsp页面
        servletContext.setAttribute("mydata", "this is mydata from myDayaServletContextListener");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        this.servletContext = null;
    }
}
