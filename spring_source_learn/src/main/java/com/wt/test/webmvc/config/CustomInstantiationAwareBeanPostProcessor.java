package com.wt.test.webmvc.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;

import java.beans.PropertyDescriptor;

/**
 * spring揭秘  page88页提示部分
 * bean初始化短路，即提前返回bean，以下某个方法返回非null时，bean会提前返回。
 * 容器会首先检查容器中是否注册有InstantiationAwareBeanPostProcessor类型的BeanPostProcessor。
 * 如果有，首先使用相应的InstantiationAwareBeanPostProcessor来构造对象实例。构造成功后直接返回构造完成的对象实例，而不会按照“正
 * 规的流程”继续执行。这就是它可能造成“短路”的原因
 * @author: wangtao
 * @date:15:29 2019/3/1
 * @email:taow02@jumei.com
 */
public class CustomInstantiationAwareBeanPostProcessor implements InstantiationAwareBeanPostProcessor {

	@Override
	public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
		System.out.println("----------------------a");
		return null;
	}

	@Override
	public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
		System.out.println("----------------------b");
		//这里返回true或者false，表示如果还有其他的Processor处理器的时候，是否继续丢给其他的处理器再次处理，
		//如果这里返回false，系统进不能启动，因为entityManager有后续处理器，这里返回false之后，就不会再丢给其他处理器处理了，应用就会报错
		return true;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("----------------------d");
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("----------------------e");
		return bean;
	}
}
