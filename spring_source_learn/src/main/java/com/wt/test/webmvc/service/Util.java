package com.wt.test.webmvc.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service
public class Util implements ApplicationContextAware{

	private static ApplicationContext applicationContext;

	public Util() {
	}

	// spring 3.2.10版本不支持此种方式，需要实现aware接口和无参构造函数
	// 但是5.1.5就支持此种构造注入了，就不用实现aware接口了
	public Util(ApplicationContext applicationContext) {
		Util.applicationContext=applicationContext;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		Util.applicationContext = applicationContext;
	}
}
