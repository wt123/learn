package com.wt.test.customtag;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * @description
 * @author: wangtao
 * @date:14:56 2019/5/21
 * @email:taow02@jumei.com
 */
public class MyNamespaceHandler extends NamespaceHandlerSupport {
	public void init() {
		registerBeanDefinitionParser("user", new UserBeanDefinitionParser());
	}
}
