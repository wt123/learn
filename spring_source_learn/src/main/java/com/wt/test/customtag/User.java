package com.wt.test.customtag;

/**
 * @description
 * @author: wangtao
 * @date:11:30 2019/5/21
 * @email:taow02@jumei.com
 */
public class User {

	private String name;
	private int age;
	private String email;

	public String getName() {
		return name;
	}

	public User setName(String name) {
		this.name = name;
		return this;
	}

	public int getAge() {
		return age;
	}

	public User setAge(int age) {
		this.age = age;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public User setEmail(String email) {
		this.email = email;
		return this;
	}

	@Override
	public String toString() {
		return "User{" + "name='" + name + '\'' + ", age=" + age + ", email='" + email + '\'' + '}';
	}
}
