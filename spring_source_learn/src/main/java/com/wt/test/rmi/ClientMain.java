package com.wt.test.rmi;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @description
 * @author: wangtao
 * @date:17:31 2019/5/31
 * @email:386427665@qq.com
 */
public class ClientMain {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext x = new ClassPathXmlApplicationContext("application-rmi-client.xml");
        HelloRMIService b = (HelloRMIService) x.getBean("myClient");
        System.out.println("2 + 3 = " + b.getAdd(2, 3));
    }
}
