package com.wt.test.rmi;

/**
 * @description
 * @author: wangtao
 * @date:17:23 2019/5/31
 * @email:386427665@qq.com
 */
public interface HelloRMIService {

    int getAdd(int a, int b);

}
