package com.wt.test.rmi;

/**
 * @description
 * @author: wangtao
 * @date:17:23 2019/5/31
 * @email:386427665@qq.com
 */
public class HelloRMIServiceImpl implements HelloRMIService{

    @Override
    public int getAdd(int a, int b) {
        return a + b;
    }

}
