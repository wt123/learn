package com.wt.test.rmi;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @description
 * @author: wangtao
 * @date:17:31 2019/5/31
 * @email:386427665@qq.com
 */
public class ServerMain {

    public static void main(String[] args) {
        new ClassPathXmlApplicationContext("application-rmi-server.xml");
    }
}
