package com.wt.test.lookup;

import org.springframework.beans.factory.support.MethodReplacer;

import java.lang.reflect.Method;

/**
 * @description
 * @author: wangtao
 * @date:15:54 2019/5/23
 * @email:taow02@jumei.com
 */
public class DogReplacer implements MethodReplacer {
	@Override
	public Object reimplement(Object obj, Method method, Object[] args) throws Throwable {
		System.out.println("replacer: this is black dog");
		return null;
	}
}
