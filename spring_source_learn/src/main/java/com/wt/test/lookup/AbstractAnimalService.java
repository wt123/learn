package com.wt.test.lookup;

/**
 * @description
 * @author: wangtao
 * @date:15:49 2019/5/23
 * @email:taow02@jumei.com
 */
public abstract class AbstractAnimalService implements AnimalService {

	public void getMe() {
		getAnimal().getMe();
	}

}
