package com.wt.test.lookup;

/**
 * @description
 * @author: wangtao
 * @date:15:26 2019/5/23
 * @email:taow02@jumei.com
 */
public interface AnimalService {
	Animal getAnimal();
}
