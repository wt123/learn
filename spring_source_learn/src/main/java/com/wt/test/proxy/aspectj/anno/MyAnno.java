package com.wt.test.proxy.aspectj.anno;

import java.lang.annotation.*;

/**
 * @description
 * @author: wangtao
 * @date:11:34 2019/3/5
 * @email:taow02@jumei.com
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface MyAnno {
	String value() default "defaultValue";
}
