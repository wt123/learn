package com.wt.test.proxy.aspectj.anno;

/**
 * @description
 * @author: wangtao
 * @date:11:36 2019/3/5
 * @email:taow02@jumei.com
 */
public class FooAnno {

	@MyAnno
	public void method1(User user){
		System.out.println("FooAnno(User)");
	}

	public void method2(UserWithoutAnno userWithoutAnno){
		System.out.println("FooAnno(UserWithoutAnno)");
	}

}
