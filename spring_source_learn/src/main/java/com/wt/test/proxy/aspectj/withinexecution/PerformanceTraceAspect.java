package com.wt.test.proxy.aspectj.withinexecution;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * execution:接受方法声明
 * within:只接受类型声明,代理目标类的所有方法，当然，可以加限定条件，单demo
 * 两关键字的表达式都支持*，..，这种形式
 * eg:within(com.wt.)
 * @author: wangtao
 * @date:10:21 2019/3/5
 * @email:taow02@jumei.com
 */
@Aspect
public class PerformanceTraceAspect {

	//限定代理Foo类型的所有method1方法
	@Pointcut("within(com.wt.test.proxy.aspectj.withinexecution.Foo) && execution(public void *.method1())")
	public void pointCutName(){}

	@Around("pointCutName()")
	public Object performanceTrance(ProceedingJoinPoint joinPoint) throws Throwable{
		System.out.println("before");
		Object o = joinPoint.proceed();
		System.out.println("after");
		return o;
	}

}
