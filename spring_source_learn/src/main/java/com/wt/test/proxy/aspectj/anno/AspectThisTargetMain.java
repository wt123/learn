package com.wt.test.proxy.aspectj.anno;

import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;

/**
 * @description
 * @author: wangtao
 * @date:10:43 2019/3/5
 * @email:taow02@jumei.com
 */
public class AspectThisTargetMain {
	public static void main(String[] args) {
		anno();
	}

	public static void anno() {
		FooAnno target = new FooAnno();
		AspectJProxyFactory weaver = new AspectJProxyFactory();
		weaver.setProxyTargetClass(true);
		weaver.setTarget(target);
		weaver.addAspect(PerformanceAnnoAspect.class);
		FooAnno foo = weaver.getProxy();
		foo.method1(new User());
		foo.method2(new UserWithoutAnno());
	}

}
