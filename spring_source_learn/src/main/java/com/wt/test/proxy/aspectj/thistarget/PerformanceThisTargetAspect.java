package com.wt.test.proxy.aspectj.thistarget;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * spring揭秘 page205 10.1.2
 * this和target的用法还有另一种，this对象调用target对象的方法时才代理，其他对象调用target不代理，spring AOP中的this和target跟Aspectj中的这两个关键字含义不一样，详细看spring揭秘
 * 以下为springAOP中的this和target含义
 * this:目标对象的代理对象
 * target:目标对象
 * this(ObjectType)和target(ObjectType):表示代理对象必须是ObjectType子类，目标对象必须是ObjectType的子类，
 * ObjectType相当于是起个标记作用的接口或类，通常用接口，因为类只能继承一个，而接口可以多实现
 * 因为代理对象一定有目标对象实现的接口或者父类，所以，这里的this和target用法意义区别不大，即这里表达式即使是两个this或者两个target效果都是一样的
 * 这种代理的是目标类所有的方法，当然可以再使用execution表达式增加限定条件
 * @author: wangtao
 * @date:10:21 2019/3/5
 * @email:taow02@jumei.com
 */
@Aspect
public class PerformanceThisTargetAspect {

	@Pointcut("execution(public void *.method1()) && this(ProxyFlagInterface1) && target(ProxyFlagInterface2)")
	public void pointCutName2() {
	}

	@Around("pointCutName2()")
	public Object performanceTrance2(ProceedingJoinPoint joinPoint) throws Throwable {
		System.out.println("this&target before");
		Object o = joinPoint.proceed();
		System.out.println("this&target after");
		return o;
	}

}
