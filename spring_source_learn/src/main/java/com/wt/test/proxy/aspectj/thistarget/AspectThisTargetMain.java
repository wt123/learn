package com.wt.test.proxy.aspectj.thistarget;

import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;

/**
 * @description
 * @author: wangtao
 * @date:10:43 2019/3/5
 * @email:taow02@jumei.com
 */
public class AspectThisTargetMain {
	public static void main(String[] args) {
		thisTargetFooTargetOne();
		thisTargetFooTargetTwo();
	}

	public static void thisTargetFooTargetOne() {
		FooTargetOne target = new FooTargetOne();
		AspectJProxyFactory weaver = new AspectJProxyFactory();
		weaver.setProxyTargetClass(true);
		weaver.setTarget(target);
		weaver.addAspect(PerformanceThisTargetAspect.class);
		FooTargetOne foo = weaver.getProxy();
		foo.method1();
		foo.method2();
	}

	public static void thisTargetFooTargetTwo() {
		FooTargetTwo target = new FooTargetTwo();
		AspectJProxyFactory weaver = new AspectJProxyFactory();
		weaver.setProxyTargetClass(true);
		weaver.setTarget(target);
		weaver.addAspect(PerformanceThisTargetAspect.class);
		FooTargetTwo foo = weaver.getProxy();
		foo.method1();
		foo.method2();
	}

}
