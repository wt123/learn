package com.wt.test.proxy.aspectj.withinexecution;

/**
 * @description
 * @author: wangtao
 * @date:9:55 2019/3/5
 * @email:taow02@jumei.com
 */
public class Foo {

	public void method1() {
		System.out.println("Foo.method1()");
	}

	public void method2() {
		System.out.println("Foo.method2()");
	}

}
