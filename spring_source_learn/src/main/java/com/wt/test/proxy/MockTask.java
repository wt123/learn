package com.wt.test.proxy;

/**
 * @description
 * @author: wangtao
 * @date:16:32 2019/3/4
 * @email:taow02@jumei.com
 */
public class MockTask implements ITask {
	@Override
	public void sayHello(String hello) {
		System.out.println("hello " + hello);
	}
}
