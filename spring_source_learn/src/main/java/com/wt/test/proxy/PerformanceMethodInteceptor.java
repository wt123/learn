package com.wt.test.proxy;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.util.StopWatch;

/**
 * @description
 * @author: wangtao
 * @date:16:28 2019/3/4
 * @email:taow02@jumei.com
 */
public class PerformanceMethodInteceptor implements MethodInterceptor {
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		StopWatch stopWatch = new StopWatch();
		try {
			stopWatch.start();
			System.out.println("before");
			Object proceed = invocation.proceed();
			System.out.println("after");
			return proceed;
		} finally {
			stopWatch.stop();
		}
	}
}
