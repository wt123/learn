package com.wt.test.proxy.aspectj.thistarget;

/**
 * @description
 * @author: wangtao
 * @date:10:40 2019/3/5
 * @email:taow02@jumei.com
 */
public class FooTargetTwo implements ProxyFlagInterface1{

	public void method1(){
		System.out.println("FooTargetTwo.method1()");
	}
	public void method2(){
		System.out.println("FooTargetOne.method2()");
	}
}
