package com.wt.test.proxy.aspectj.withinexecution;

import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;

/**
 * @description
 * @author: wangtao
 * @date:9:53 2019/3/5
 * @email:taow02@jumei.com
 */
public class AspectJAopMain {

	public static void main(String[] args) {
		execution();
	}

	public static void execution() {
		Foo target = new Foo();
		AspectJProxyFactory weaver = new AspectJProxyFactory();
		weaver.setProxyTargetClass(true);
		weaver.setTarget(target);
		weaver.addAspect(PerformanceTraceAspect.class);
		Foo foo = weaver.getProxy();
		foo.method1();
		foo.method2();
	}


}
