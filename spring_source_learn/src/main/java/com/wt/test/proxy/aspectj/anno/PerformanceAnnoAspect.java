package com.wt.test.proxy.aspectj.anno;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * spring揭秘 page205 10.1.2
 * @author: wangtao
 * @date:10:21 2019/3/5
 * @email:taow02@jumei.com
 */
//spring揭秘 page205 10.1.2
//args：根据参数匹配,args(User),代理入参为User的所有方法;
//args,@args,this,target,@within,@target,@annotation这种原本是指定对象类型的，都可以像args(参数名)来绑定入参
//@args:根据参数含有的注解匹配，@args(Myanno),如果入参类型拥有Myanno注解，就代理，这种情况匹配的是调用的时候传递进来的参数，所以传递如果为null，会报空指针异常
//同类型的还有@target,@within，@annotation，这些同类型的都是@开头的
//@target:目标类包含指定的注解，就代理，目标是类，看似跟@within没啥区别，这个注解基本上用于跟其他的表达式一并使用，用于加强匹配规则用，跟target和this一样
//@within:目标类拥有指定的注解，就代理，目标是类
//@annotation：目标对象的方法拥有指定注解，就代理，目标是方法
@Aspect
public class PerformanceAnnoAspect {

	//	@Pointcut("@args(MyAnno)")
	@Pointcut("args(User) && args(user) && @annotation(myAnno)")
	public void pointCutName(User user, MyAnno myAnno) {
	}

	@Around("pointCutName(user,myAnno)")
	public Object performanceTrance2(ProceedingJoinPoint joinPoint, User user, MyAnno myAnno) throws Throwable {
		System.out.println("anno before:" + myAnno.value());
		Object o = joinPoint.proceed();
		System.out.println("anno after:" + myAnno.value());
		return o;
	}

}
