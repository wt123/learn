package com.wt.test.mybatis;

import com.wt.test.jdbc.UserEntity;

import java.util.List;

/**
 * @description
 * @author: wangtao
 * @date:17:24 2019/5/28
 * @email:taow02@jumei.com
 */
public interface UserEntityMapper {

	void insertUser(UserEntity user);

	List<UserEntity> getUser(String name);

}
