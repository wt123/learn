package com.wt.myaop.runner;

import com.wt.myaop.property.MyProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;

/**
 * @author wangtao
 * @date 2023/3/25 11:30
 **/
public class MyPropertiesPrintRunner implements ApplicationRunner, Ordered {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final MyProperties myProperties;

    public MyPropertiesPrintRunner(MyProperties myProperties) {
        this.myProperties = myProperties;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("From my args monitor starter, MyProperties: {} ", myProperties);
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
