package com.wt.myaop.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @description
 * @author: wangtao
 * @date:18:19 2019/6/4
 * @email:386427665@qq.com
 */
@ConfigurationProperties(prefix = MyProperties.MY_PREFIX)
public class MyProperties {
    public static final String MY_PREFIX = "myconfig";

    /**
     * 不加该注解也能正常得到值
     */
    @NestedConfigurationProperty
    private UserProperties user;

    private String job;

    public UserProperties getUser() {
        return user;
    }

    public void setUser(UserProperties user) {
        this.user = user;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    @Override
    public String toString() {
        return "MyConfig{" +
                "user=" + user +
                ", job='" + job + '\'' +
                '}';
    }
}
