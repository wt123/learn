package com.wt.myaop;

import com.wt.myaop.config.MyAroundConfiguration;
import com.wt.myaop.property.MyProperties;
import com.wt.myaop.runner.MyPropertiesPrintRunner;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @description
 * @author: wangtao
 * @date:17:44 2019/6/4
 * @email:386427665@qq.com
 */
@Configuration(proxyBeanMethods = false)
@AutoConfigureAfter(AopAutoConfiguration.class)
@EnableConfigurationProperties(MyProperties.class)
public class MyArgsMonitorAutoConfiguration {

    private MyProperties myProperties;

    public MyArgsMonitorAutoConfiguration(MyProperties myProperties) {
        this.myProperties = myProperties;
    }

    @Bean
    public MyPropertiesPrintRunner getMyConfigService() {
        return new MyPropertiesPrintRunner(myProperties);
    }

    @Import({MyAroundConfiguration.class})
    public static class MyArgsMonitorConfig {
    }

}
