package com.wt.myaop.anno;

import java.lang.annotation.*;

/**
 * @description
 * @author: wangtao
 * @date:17:22 2019/6/4
 * @email:386427665@qq.com
 */
@Target(ElementType.METHOD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {

}
