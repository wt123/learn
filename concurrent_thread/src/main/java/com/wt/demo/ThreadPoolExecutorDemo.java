package com.wt.demo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @description
 * @author: wangtao
 * @date:11:07 2018/5/31
 * @email:tao8.wang@changhong.com
 */
public class ThreadPoolExecutorDemo implements Runnable {

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            System.out.println(System.currentTimeMillis() + ":" + Thread.currentThread().getId());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService =null;
        executorService = Executors.newCachedThreadPool();
//        executorService = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 10; i++) {
            executorService.submit(new Thread(new ThreadPoolExecutorDemo()));
        }
    }
}
