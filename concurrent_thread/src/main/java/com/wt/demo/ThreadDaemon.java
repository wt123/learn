package com.wt.demo;

/**
 * @description
 * @author: wangtao
 * @date:14:17 2019/6/11
 * @email:386427665@qq.com
 */
public class ThreadDaemon {

    public static class TDaemon extends Thread {
        @Override
        public void run() {
            while (true) {
                try {
                    System.out.println("aaaaa");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        TDaemon t = new TDaemon();
        // 如果不设置成守护线程，则t线程就是用户线程，不会随mian线程的结束而结束，会永远循环下去
        // 设置成守护线程后，当主线程结束后，该线程就自动退出了
        t.setDaemon(true);
        t.start();
        Thread.sleep(3000);
    }

}
