package com.wt.demo;

/**
 * @description
 * @author: wangtao
 * @date:14:17 2019/6/11
 * @email:386427665@qq.com
 */
public class ThreadVolatile {

    private static volatile boolean ready;
    private static int number;

    public static class T extends Thread {
        @Override
        public void run() {
            while (!ready) {
            }
            System.out.println(number);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new T().start();
        Thread.sleep(1000);
        number = 42;
        ready = true;
        Thread.sleep(3000);
    }

}
