package com.wt.demo.threadtest.future;

/**
 * @description
 * @author: wangtao
 * @date:9:26 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class Client {
    public Data request(final Object obj) {
        final FutureData futureData = new FutureData();
        new Thread() {
            @Override
            public void run() {
                RealData realData = new RealData();
                realData.setData(obj);
                futureData.setRealData(realData);
            }
        }.start();
        return futureData;
    }
}
