package com.wt.demo.threadtest.frame.test;

import com.wt.demo.threadtest.frame.Operate;

/**
 * @description
 * @author: wangtao
 * @date:13:00 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class FourthOperate extends Operate {
    @Override
    public Object operate() {
        Object o = null;
        try {
            o = this.queue.take();
            Thread.sleep(1000);
            System.out.println("this is FourthOperate" + o.toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return o;
    }
}
