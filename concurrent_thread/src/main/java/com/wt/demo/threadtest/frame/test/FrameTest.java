package com.wt.demo.threadtest.frame.test;

import com.wt.demo.threadtest.frame.OnFinishListener;
import com.wt.demo.threadtest.frame.OperateChain;
import com.wt.demo.threadtest.frame.OperateExecutorManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @description
 * @author: wangtao
 * @date:13:04 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class FrameTest {

    static BlockingQueue<String> result = new LinkedBlockingDeque<>();

    public static void main(String[] args) {
        //构造所有需要的流水线
        FirstOperate first = new FirstOperate();
        first.setQueue(new LinkedBlockingDeque<Object>());
        SecondOperate second = new SecondOperate();
        second.setQueue(new LinkedBlockingDeque<Object>());
        ThirdOperate third = new ThirdOperate();
        third.setQueue(new LinkedBlockingDeque<Object>());
        FourthOperate fourth = new FourthOperate();
        fourth.setQueue(new LinkedBlockingDeque<Object>());
        OnFinishListener listener1 = (obj) -> System.out.println("finished result is " + obj.toString() + " from listener1");
        //用一个队列去监听，处理结果放到result队列中
        OnFinishListener listener2 = (obj) -> result.add(obj.toString());
        fourth.setListeners(Arrays.asList(listener1, listener2));

        //开始添加并执行流水线
        OperateExecutorManager.addOperates(Arrays.asList(first, third, second, fourth));
        OperateExecutorManager.initOperate();
        //100个数据需要流水线处理
        for (int i = 0; i < 100; i++) {
            OperateExecutorManager.handle(i);
        }

        //取出结果
        try {
            while (true) {
                System.out.println(result.take());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
