package com.wt.demo.threadtest.future;

/**
 * @description
 * @author: wangtao
 * @date:9:12 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public interface Data {

    Object getData();

}
