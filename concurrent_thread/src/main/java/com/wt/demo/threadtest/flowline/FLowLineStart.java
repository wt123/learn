package com.wt.demo.threadtest.flowline;

/**
 * @description
 * @author: wangtao
 * @date:11:01 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class FLowLineStart {
    public static long startTime = 0;

    public static void main(String[] args) throws InterruptedException {
        flowLine();
    }

    public static void flowLine() throws InterruptedException {
        new Thread(new AddOperate()).start();
        new Thread(new MultiplyOperate()).start();
        new Thread(new DivOperate()).start();
        long start = System.currentTimeMillis();
        System.out.println(start);
        startTime = start;
        for (int i = 1; i < 100; i++) {
            AddOperate.addQueue.add(new Integer[]{i, i});
        }
    }

    public static void notFlowLine() {
        //10922
        long start = System.currentTimeMillis();
        startTime = start;
        for (int i = 1; i < 100; i++) {
            int result = i + i;
            delay();
            result = result * result;
            delay();
            double r = Math.sqrt(result) + 1;
            System.out.println(System.currentTimeMillis() - startTime);
        }
    }

    public static void delay() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}