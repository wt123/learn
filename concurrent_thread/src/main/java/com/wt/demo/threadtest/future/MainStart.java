package com.wt.demo.threadtest.future;

/**
 * @description
 * @author: wangtao
 * @date:9:30 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class MainStart {
    public static void main(String[] args) throws InterruptedException {
        Client c=new Client();
        Data data = c.request("12345");
        System.out.println(data.getData());
    }
}
