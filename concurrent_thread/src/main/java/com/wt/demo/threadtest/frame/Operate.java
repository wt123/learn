package com.wt.demo.threadtest.frame;

import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * @description
 * @author: wangtao
 * @date:11:34 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public abstract class Operate implements Order {

    protected BlockingQueue<Object> queue;

    protected Operate nextOperate;

    protected List<OnFinishListener> listeners;

    public BlockingQueue<Object> getQueue() {
        return queue;
    }

    public void setQueue(BlockingQueue<Object> queue) {
        this.queue = queue;
    }

    public Operate() {
    }

    public Operate getNextOperate() {
        return nextOperate;
    }

    public void setNextOperate(Operate nextOperate) {
        this.nextOperate = nextOperate;
    }

    public List<OnFinishListener> getListeners() {
        return listeners;
    }

    public void setListeners(List<OnFinishListener> listeners) {
        this.listeners = listeners;
    }

    public Operate(Operate nextOperate) {
        this.nextOperate = nextOperate;
    }

    public abstract Object operate();

    public void doOperate() {
        Object o = operate();
        if (nextOperate != null) {
            nextOperate.getQueue().add(o);
        }
        // 操作完成通知
        if(listeners!=null)
        for (OnFinishListener listener : listeners) {
            listener.onFinish(o);
        }
    }
}
