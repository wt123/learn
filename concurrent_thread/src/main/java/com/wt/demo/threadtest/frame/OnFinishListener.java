package com.wt.demo.threadtest.frame;

/**
 * @description
 * @author: wangtao
 * @date:13:26 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public interface OnFinishListener {
    /**
     * 操作完成监听
     * @param obj
     */
    void onFinish(Object obj);
}
