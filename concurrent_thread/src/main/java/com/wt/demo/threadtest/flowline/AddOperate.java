package com.wt.demo.threadtest.flowline;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @description
 * @author: wangtao
 * @date:10:50 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class AddOperate implements Runnable {

    public static BlockingQueue<Integer[]> addQueue;

    public AddOperate() {
        addQueue = new LinkedBlockingDeque<>();
    }

    @Override
    public void run() {
        while (true) {
            try {
                //模拟耗时操作
                FLowLineStart.delay();
                Integer[] args = addQueue.take();
                int result = args[0] + args[1];
                MultiplyOperate.multiplyQueue.add(result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
