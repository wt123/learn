package com.wt.demo.threadtest.flowline;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @description
 * @author: wangtao
 * @date:10:50 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class MultiplyOperate implements Runnable {

    public static BlockingQueue<Integer> multiplyQueue;

    public MultiplyOperate() {
        multiplyQueue = new LinkedBlockingDeque<>();
    }

    @Override
    public void run() {
        while (true) {
            try {
                //模拟耗时操作
                FLowLineStart.delay();
                Integer value = multiplyQueue.take();
                int result = value * value;
                DivOperate.divQueue.offer(result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
