package com.wt.demo.threadtest.future;

/**
 * @description
 * @author: wangtao
 * @date:9:13 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class RealData implements Data {

    private Object realData;

    public void setData(Object realData) {
        StringBuffer sb = new StringBuffer();
        try {
            char[] cs = realData.toString().toCharArray();
            for (int i = cs.length - 1; i >= 0; i--) {
                Thread.sleep(1000);
                sb.append(cs[i]);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.realData = sb.toString();
    }

    @Override
    public Object getData() {
        return realData;
    }
}
