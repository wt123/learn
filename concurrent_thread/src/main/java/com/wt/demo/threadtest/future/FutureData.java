package com.wt.demo.threadtest.future;

/**
 * @description
 * @author: wangtao
 * @date:9:12 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class FutureData implements Data {

    private Data realData;
    private boolean isReady;

    public synchronized void setRealData(Data realData) {
        if (isReady) {
            System.out.println("return");
            return;
        }
        this.realData = realData;
        isReady = true;
        System.out.println("isReady:" + isReady);
        notifyAll();
    }

    @Override
    public synchronized Object getData() {
        while (!isReady) {
            System.out.println(isReady);
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return realData.getData();
    }
}
