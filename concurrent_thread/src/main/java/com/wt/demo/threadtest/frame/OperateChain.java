package com.wt.demo.threadtest.frame;

import java.util.LinkedList;
import java.util.List;

/**
 * @description
 * @author: wangtao
 * @date:11:35 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class OperateChain {

    protected List<Operate> operates;

    public OperateChain() {
        this(null);
    }

    public OperateChain(List<Operate> operates) {
        operates = new LinkedList<>();
        if (operates != null || operates.size() > 0) {
            this.operates = operates;
        }
    }

    public List<Operate> getOperates() {
        return operates;
    }

    public void setOperates(List<Operate> operates) {
        this.operates = operates;
    }

    public void addOperate(Operate operate) {
        this.operates.add(operate);
    }

}
