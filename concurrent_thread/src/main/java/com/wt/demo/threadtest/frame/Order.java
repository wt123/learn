package com.wt.demo.threadtest.frame;

/**
 * @description
 * @author: wangtao
 * @date:11:24 2019/7/19
 * @email:386427665@qq.com
 */
public interface Order {

    default int getOrder() {
        return Integer.MAX_VALUE;
    }

}
