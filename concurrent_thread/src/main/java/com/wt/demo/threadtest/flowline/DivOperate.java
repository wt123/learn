package com.wt.demo.threadtest.flowline;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @description
 * @author: wangtao
 * @date:10:50 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class DivOperate implements Runnable {

    public static BlockingQueue<Integer> divQueue;

    public DivOperate() {
        divQueue = new LinkedBlockingDeque<>();
    }

    @Override
    public void run() {
        while (true) {
            try {
                Integer value = divQueue.take();
                System.out.println(System.currentTimeMillis() - FLowLineStart.startTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
