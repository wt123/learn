package com.wt.demo.threadtest.future;

import java.util.concurrent.*;

/**
 * @description
 * @author: wangtao
 * @date:9:47 2018/6/11
 * @email:tao8.wang@changhong.com
 */
public class JDKFuture {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<String> futureTask = new FutureTask<String>(new MyRealData("12345"));
        ExecutorService es = Executors.newCachedThreadPool();
        es.submit(futureTask);
        System.out.println(futureTask.get());
    }
}

class MyRealData implements Callable<String> {

    private String realData;

    public MyRealData(String realData) {
        this.realData = realData;
    }

    @Override
    public String call() throws Exception {
        StringBuffer sb = new StringBuffer();
        try {
            char[] cs = realData.toString().toCharArray();
            for (int i = cs.length - 1; i >= 0; i--) {
                Thread.sleep(1000);
                sb.append(cs[i]);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}

