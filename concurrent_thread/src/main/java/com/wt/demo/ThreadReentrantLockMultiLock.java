package com.wt.demo;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 主线程锁两次，如果只释放一次，线程锁死，不会执行，但是在子线程中，只释放一次也可以正常执行。。。
 *
 * @author: wangtao
 * @date:10:00 2019/6/12
 * @email:386427665@qq.com
 */
public class ThreadReentrantLockMultiLock {

    static ReentrantLock lock = new ReentrantLock(true);
    static Condition condition = lock.newCondition();

    private String name;

    public static class TR1 implements Runnable {

        @Override
        public void run() {
            while (true) {
                lock.lock();
                lock.lock();
                lock.lock();
                System.out.println("t1 wait,only unlock one lock");
                lock.unlock();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class TR2 implements Runnable {

        @Override
        public void run() {
            while (true) {
                lock.lock();
                System.out.println("this is t2");
                condition.signalAll();
                lock.unlock();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public ThreadReentrantLockMultiLock(String name) {
        this.name = name;
    }


    public static void main(String[] args) throws InterruptedException {
        TR1 target = new TR1();
        lock.lock();
        lock.lock();
        new Thread(target).start();
        new Thread(target).start();
        lock.unlock();
        lock.unlock();//主线程锁两次，如果只释放一次，线程锁死，不会执行，但是在子线程中，只释放一次也可以正常执行。。。
//        new Thread(new TR2()).start();
    }
}
