package com.wt.demo;

/**
 * @description
 * @author: wangtao
 * @date:10:46 2019/6/11
 * @email:386427665@qq.com
 */
public class ThreadWaitNotify {

    final static Object obj = new Object();

    public static void main(String[] args) {
        new T1().start();
        new T2().start();
    }

    static class T1 extends Thread {
        @Override
        public void run() {
            synchronized (obj) {
                System.out.println(System.currentTimeMillis() + ": T1 start!");
                try {
                    System.out.println(System.currentTimeMillis() + ": T1 wait for obj");
                    obj.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(System.currentTimeMillis() + ": T1 end");
        }
    }

    static class T2 extends Thread {
        @Override
        public void run() {
            synchronized (obj) {
                System.out.println(System.currentTimeMillis() + ": T2 start! notify one Thread");
                try {
                    obj.notify();
                    System.out.println(System.currentTimeMillis() + ": T2 end");
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
