package com.wt.demo;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.LockSupport;

/**
 * @description
 * @author: wangtao
 * @date:11:07 2018/5/31
 * @email:tao8.wang@changhong.com
 */
public class ThreadLockSupport {

    public static Object o = new Object();
    public static Rrr t1 = new Rrr("t1");
    public static Rrr t2 = new Rrr("t2");

    public static class Rrr extends Thread {
        public Rrr(String name) {
            super.setName(name);
        }

        @Override
        public void run() {
            synchronized (o) {
                System.out.println("in " + getName());
                LockSupport.park();
                System.out.println("parting...");
                if (Thread.interrupted()) {
                    System.out.println("被中断了");
                }
            }
            System.out.println("执行结束");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        t1.start();
        t1.sleep(3000);
        t2.start();
        t1.interrupt();
        t2.sleep(3000);
        LockSupport.unpark(t2);
    }
}


