package com.wt.demo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @description
 * @author: wangtao
 * @date:13:56 2019/6/17
 * @email:386427665@qq.com
 */
public class ThreadScheduledDemo {

    public static void main(String[] args) {
        ScheduledExecutorService exec = Executors.newScheduledThreadPool(10);
        Runnable runnable = () -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(System.currentTimeMillis() / 1000);
        };
//        exec.scheduleAtFixedRate(runnable,0,2, TimeUnit.SECONDS);
        exec.scheduleWithFixedDelay(runnable,0,2, TimeUnit.SECONDS);
    }

}
