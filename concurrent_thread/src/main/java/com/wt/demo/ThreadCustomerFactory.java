package com.wt.demo;

import java.util.concurrent.*;

/**
 * @description
 * @author: wangtao
 * @date:15:51 2019/6/17
 * @email:386427665@qq.com
 */
public class ThreadCustomerFactory {

    public static void main(String[] args) throws InterruptedException {
        Runnable task = () -> {
            try {
                Thread.sleep(1000);
                System.out.println("thread-" + Thread.currentThread().getId() + ":" + System.currentTimeMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        ThreadFactory fac = (runnable) -> {
            Thread t = new Thread(runnable);
            //打印出创建线程信息，设置成守护线程，当主线程退出时强制销毁
            System.out.println("create Thread...");
            t.setDaemon(true);
            return t;
        };
        ExecutorService exec = new ThreadPoolExecutor(5, 5, 0, TimeUnit.SECONDS, new ArrayBlockingQueue<>(5), fac);
        for (int i = 0; i < 10; i++) {
            exec.execute(task);
        }
        Thread.sleep(2000);
    }

}
