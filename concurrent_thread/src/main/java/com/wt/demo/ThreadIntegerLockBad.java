package com.wt.demo;

/**
 * @description
 * @author: wangtao
 * @date:17:14 2019/6/11
 * @email:386427665@qq.com
 */
public class ThreadIntegerLockBad implements Runnable {

    static Integer i = 0;

    @Override
    public void run() {
        for (int j = 0; j < 10000000; j++) {
            synchronized (this) {
                i++;
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadIntegerLockBad target = new ThreadIntegerLockBad();
        Thread t1=new Thread(target);
        Thread t2=new Thread(target);
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(i);
    }
}
