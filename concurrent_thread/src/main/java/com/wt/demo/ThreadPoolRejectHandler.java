package com.wt.demo;

import java.util.concurrent.*;

/**
 * @description
 * @author: wangtao
 * @date:15:25 2019/6/17
 * @email:386427665@qq.com
 */
public class ThreadPoolRejectHandler {

    public static void main(String[] args) throws InterruptedException {
        Runnable task = () -> {
            try {
                Thread.sleep(1000);
                System.out.println("thread-" + Thread.currentThread().getId() + ":" + System.currentTimeMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        RejectedExecutionHandler reject = (r, executor) -> System.out.println(r.toString() + ": discard");
        ExecutorService exec = new ThreadPoolExecutor(5, 5, 0, TimeUnit.SECONDS, new ArrayBlockingQueue<>(5), Executors.defaultThreadFactory(), reject);
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            exec.execute(task);
            Thread.sleep(10);
        }
    }

}
