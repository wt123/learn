package com.wt.demo;

/**
 * @description
 * @author: wangtao
 * @date:15:01 2019/6/11
 * @email:386427665@qq.com
 */
public class ThreadSynchronizedBad implements Runnable {

    static int i = 0;

    public synchronized void incr() {
        i++;
    }

    @Override
    public void run() {
        for (int j = 0; j < 10000000; j++) {
            incr();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1=new Thread(new ThreadSynchronizedBad());
        Thread t2=new Thread(new ThreadSynchronizedBad());
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(i);
    }
}
