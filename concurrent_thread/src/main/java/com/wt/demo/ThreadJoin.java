package com.wt.demo;

/**
 * @description
 * @author: wangtao
 * @date:11:21 2019/6/11
 * @email:386427665@qq.com
 */
public class ThreadJoin {

    public volatile static int i=0;

    public static class AddThread extends Thread{
        @Override
        public void run() {
            for(;i<10000;i++){}
        }
    }

    public static void main(String[] args) throws InterruptedException {
        AddThread t=new AddThread();
        t.start();
        t.join();
        System.out.println(i);
    }
}
