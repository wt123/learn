package com.wt.demo;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 重入锁，公平锁
 * @author: wangtao
 * @date:10:00 2019/6/12
 * @email:386427665@qq.com
 */
public class ThreadReentrantLock implements Runnable{

    static ReentrantLock lock = new ReentrantLock(true);

    private String name;

    public ThreadReentrantLock(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        while(true){
            lock.lock();
            try {
                System.out.println(name +" get lock");
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.unlock();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new ThreadReentrantLock("t1"));
        Thread t2 = new Thread(new ThreadReentrantLock("t2"));
        t1.start();
        t2.start();
        t1.join();
        t2.join();
    }
}
