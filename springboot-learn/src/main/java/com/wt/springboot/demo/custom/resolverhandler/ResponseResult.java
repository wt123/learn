package com.wt.springboot.demo.custom.resolverhandler;

import com.wt.springboot.demo.custom.components.requestmonitor.RequestMonitor;

/**
 * @author wangtao
 * @date 2023/3/5 19:08
 **/
public class ResponseResult<T> implements RequestMonitor, java.io.Serializable {
    private Integer code;
    private String msg;
    private T data;

    private String traceId;
    private Long timeUse;

    public static <T> ResponseResult<T> success(T data) {
        ResponseResult<T> ret = new ResponseResult<>();
        ret.code = 0;
        ret.data = data;
        return ret;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String getTraceId() {
        return traceId;
    }

    @Override
    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    @Override
    public Long getTimeUse() {
        return timeUse;
    }

    @Override
    public void setTimeUse(Long timeUse) {
        this.timeUse = timeUse;
    }
}
