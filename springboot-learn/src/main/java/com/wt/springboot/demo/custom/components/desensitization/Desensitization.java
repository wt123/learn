package com.wt.springboot.demo.custom.components.desensitization;

import java.lang.annotation.*;

/**
 * 数据脱敏
 *
 * @author wangtao
 * @date 2023/3/15 20:17
 **/
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Desensitization {


}
