package com.wt.springboot.demo.controller;

import com.wt.springboot.demo.custom.resolverhandler.MyResponse;
import com.wt.springboot.demo.custom.resolverhandler.ResponseResult;
import com.wt.springboot.demo.custom.resolverhandler.Token2UserId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangtao
 * @date 2023/3/1 22:54
 **/
@Controller
public class TestController {

    @GetMapping("/test100")
    @ResponseBody
    public String test(@RequestParam("${token.name:token}") String token) {
        return "xxx";
    }

    @PostMapping("/testBody")
    @ResponseBody
    public String test(@RequestBody A a) {
        return "xxx";
    }

    @GetMapping("/testResolverHandler")
    @MyResponse
    public ResponseResult<String> testResolverHandler(@Token2UserId String userId) {
        return ResponseResult.success(userId);
    }


    public static class A {
        private String token;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }

}
