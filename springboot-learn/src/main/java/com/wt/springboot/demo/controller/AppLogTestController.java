package com.wt.springboot.demo.controller;

import com.wt.springboot.demo.custom.resolverhandler.MyResponse;
import com.wt.springboot.demo.custom.resolverhandler.ResponseResult;
import com.wt.springboot.demo.custom.resolverhandler.Token2UserId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangtao
 * @date 2023/3/1 22:54
 **/
@Controller
@RequestMapping("/appLog")
public class AppLogTestController {

    @PostMapping("/test")
    @ResponseBody
    public B test(@RequestParam("name") String name, @RequestBody A a) {
        return new B(name, a.age);
    }

    public static class A {
        private Integer age;

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
    }

    public static class B {

        private String name;
        private Integer age;

        public B(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
    }

}
