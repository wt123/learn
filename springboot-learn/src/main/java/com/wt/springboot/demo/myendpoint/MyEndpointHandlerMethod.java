package com.wt.springboot.demo.myendpoint;

import java.lang.reflect.Method;

/**
 * @author wangtao
 * @date 2023/2/27 20:08
 **/
public class MyEndpointHandlerMethod {

    /**
     * 目标方法，相当于我们写的Controller中的接口方法
     */
    private Method method;
    /**
     * Endpoint的bean，相当于我们写的Controller类
     */
    private Object bean;

    public MyEndpointHandlerMethod(Method method, Object bean) {
        this.method = method;
        this.bean = bean;
    }

    public Method getMethod() {
        return method;
    }

    public Object getBean() {
        return bean;
    }
}
