package com.wt.springboot.demo.custom.resolverhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author wangtao
 * @date 2023/3/5 18:41
 **/
@Configuration(proxyBeanMethods = false)
public class ResolverHandlerConfiguration {

    @Bean
    public WebMvcConfigurer myWebMvcConfigurer(@Autowired MyCustomArgumentResolver myCustomArgumentResolver) {
        return new WebMvcConfigurer() {
            @Override
            public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
                // 把我们自定义的请求参数解析器添加到解析器列表中
                argumentResolvers.add(0, myCustomArgumentResolver);
            }

            @Override
            public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> handlers) {
                // 把我们自定义的返回值处理器添加到返回值处理器列表中
                handlers.add(0, new MyCustomReturnValueHandler());
            }
        };
    }

}
