package com.wt.springboot.demo.custom.resolverhandler;

import java.lang.annotation.*;

/**
 * @author wangtao
 * @date 2023/3/5 19:08
 **/
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyResponse {
}
