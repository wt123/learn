package com.wt.springboot.demo.myendpoint;

import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.HandlerMapping;

/**
 * @author wangtao
 * @date 2023/2/27 19:52
 **/
@Configuration(proxyBeanMethods = false)
public class MyEndpointConfiguration {

    @Bean
    public HandlerAdapter myEndpointHandlerAdapter() {
        return new MyEndpointHandlerAdapter();
    }

    @Bean
    public HandlerMapping myEndpointHandlerMapping() {
        return new MyEndpointHandlerMapping();
    }

    @Bean
    public SmartInitializingSingleton myEndpointSmartInitializingSingleton(@Autowired MyEndpointHandlerMapping handlerMapping) {
        return handlerMapping::initMappings;
    }

}
