package com.wt.springboot.demo.custom.components.applog;

/**
 * @author wangtao
 * @date 2023/3/11 10:39
 **/
public class AppLogContextHolder {

    private static final ThreadLocal<AppLog> TL = ThreadLocal.withInitial(AppLog::new);

    public static AppLog get() {
        return TL.get();
    }

    public static void remove() {
        TL.remove();
    }

}
