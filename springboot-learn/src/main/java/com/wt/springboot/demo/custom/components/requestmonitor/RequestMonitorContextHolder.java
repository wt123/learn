package com.wt.springboot.demo.custom.components.requestmonitor;

import org.springframework.util.StopWatch;

/**
 * @author wangtao
 * @date 2023/3/11 21:35
 **/
public class RequestMonitorContextHolder {

    private static final ThreadLocal<StopWatch> TL = ThreadLocal.withInitial(StopWatch::new);

    public static StopWatch get() {
        return TL.get();
    }

    public static void remove() {
        TL.remove();
    }

}
