package com.wt.springboot.demo.custom.components.applog;

import java.util.Map;

/**
 * @author wangtao
 * @date 2023/3/11 10:40
 **/
public class AppLog {

    private String path;

    private Map<String, String[]> parameters;

    private Object reqBody;

    private Object respBody;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Map<String, String[]> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String[]> parameters) {
        this.parameters = parameters;
    }

    public Object getReqBody() {
        return reqBody;
    }

    public void setReqBody(Object reqBody) {
        this.reqBody = reqBody;
    }

    public Object getRespBody() {
        return respBody;
    }

    public void setRespBody(Object respBody) {
        this.respBody = respBody;
    }
}
