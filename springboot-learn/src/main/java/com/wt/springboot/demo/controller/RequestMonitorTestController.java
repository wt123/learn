package com.wt.springboot.demo.controller;

import com.wt.springboot.demo.custom.resolverhandler.ResponseResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

/**
 * @author wangtao
 * @date 2023/3/1 22:54
 **/
@RestController
@RequestMapping("/requestMonitor")
public class RequestMonitorTestController {

    @GetMapping("/test")
    public ResponseResult<Boolean> test() {
        try {
            // 随机模拟耗时操作
            Thread.sleep(new Random().nextInt(5000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResponseResult.success(Boolean.TRUE);
    }

}
