package com.wt.springboot.demo.custom.resolverhandler;

import java.lang.annotation.*;

/**
 * @author wangtao
 * @date 2023/3/5 18:30
 **/
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Token2UserId {
}
