package com.wt.springboot.demo.myendpoint.anno;

import java.lang.annotation.*;

/**
 * @author wangtao
 * @date 2023/2/27 19:51
 **/
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface MyEndPoint {

    String path() default "";

}
