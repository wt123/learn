package com.wt.springboot.demo.custom.components.requestmonitor;

/**
 * @author wangtao
 * @date 2023/3/11 21:49
 **/
public interface RequestMonitor {

    void setTraceId(String traceId);

    String getTraceId();

    void setTimeUse(Long timeUse);

    Long getTimeUse();

}
