package com.wt.feign.service;

import com.wt.feign.service.fallbackimpl.FeignFallBackServiceImpl;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description
 * @Author: wangtao
 * @Date:9:23 2017/6/8
 * @Email:tao8.wang@changhong.com
 */
@FeignClient(name = "eurake-services-name", path = "/servicesForFeign", fallback = FeignFallBackServiceImpl.class)
public interface FeignFallBackService {

    @RequestMapping(value = "/getttt")
    public String get(@RequestParam("key1") String value1, @RequestParam("key2") String value2);

}
