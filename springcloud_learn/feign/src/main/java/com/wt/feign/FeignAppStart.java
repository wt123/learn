package com.wt.feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * @Description
 * @Author: wangtao
 * @Date:13:20 2017/6/7
 * @Email:tao8.wang@changhong.com
 */
@SpringBootApplication
@EnableFeignClients
//该注解和@EnableEurekaClient一样
@EnableDiscoveryClient
public class FeignAppStart {
    public static void main(String[] args) {
        SpringApplication.run(FeignAppStart.class, args);
    }
}
