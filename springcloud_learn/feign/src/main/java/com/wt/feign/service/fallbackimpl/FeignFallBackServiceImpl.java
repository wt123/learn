package com.wt.feign.service.fallbackimpl;

import com.wt.feign.service.FeignFallBackService;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author: wangtao
 * @Date:9:19 2017/6/8
 * @Email:tao8.wang@changhong.com
 */
@Component
public class FeignFallBackServiceImpl implements FeignFallBackService {

    @Override
    public String get(String value1, String value2) {
        return "fallback:key1=" + value1 + ",key2=" + value2;
    }
}
