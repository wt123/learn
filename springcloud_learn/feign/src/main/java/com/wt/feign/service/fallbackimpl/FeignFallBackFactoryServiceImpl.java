package com.wt.feign.service.fallbackimpl;

import com.wt.feign.service.FeignFallBackFactoryService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author: wangtao
 * @Date:9:29 2017/6/8
 * @Email:tao8.wang@changhong.com
 */
@Component
public class FeignFallBackFactoryServiceImpl implements FallbackFactory<FeignFallBackFactoryService> {
    @Override
    public FeignFallBackFactoryService create(final Throwable throwable) {
        return new FeignFallBackFactoryService() {

            @Override
            public String get(String value1, String value2) {
                return "fallbackFactory:key1=" + value1 + ",key2=" + value2 + ";Error=" + throwable.getMessage();
            }
        };
    }
}
