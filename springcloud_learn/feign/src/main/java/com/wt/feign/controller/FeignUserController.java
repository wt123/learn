package com.wt.feign.controller;

import com.wt.feign.service.FeignUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description
 * @Author: wangtao
 * @Date:13:30 2017/6/7
 * @Email:tao8.wang@changhong.com
 */
@RestController
public class FeignUserController {

    @Autowired
    private FeignUserService fus;

    @RequestMapping("/")
    public String get() {
        String str = fus.getFeignData("67");
        return str;
    }

    @RequestMapping("/get")
    public String get(@RequestParam("a") String a, @RequestParam("b") String b) {
        String str = fus.get(a, b);
        return str;
    }

    @RequestMapping("/post")
    public String post(@RequestBody String text) {
        String str = fus.post(text);
        return str;
    }

}
