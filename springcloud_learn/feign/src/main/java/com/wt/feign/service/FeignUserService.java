package com.wt.feign.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @Description
 * @Author: wangtao
 * @Date:13:26 2017/6/7
 * @Email:tao8.wang@changhong.com
 */
@FeignClient(name = "eurake-services-name", path = "/servicesForFeign")
public interface FeignUserService {

    @RequestMapping("/pathData/{id}")
    public String getFeignData(@PathVariable("id") String id);

    @RequestMapping(value = "/get")
    public String get(@RequestParam("key1") String value1, @RequestParam("key2") String value2);

    @RequestMapping(value = "/post")
    public String post(@RequestBody String text);

}
