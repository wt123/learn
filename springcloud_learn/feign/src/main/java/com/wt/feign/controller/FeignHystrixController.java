package com.wt.feign.controller;

import com.wt.feign.service.FeignFallBackFactoryService;
import com.wt.feign.service.FeignFallBackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author: wangtao
 * @Date:9:40 2017/6/8
 * @Email:tao8.wang@changhong.com
 */
@RestController
public class FeignHystrixController {

    @Autowired
    private FeignFallBackService feignFallBackService;

    @Autowired
    private FeignFallBackFactoryService feignFallBackFactoryService;

    @RequestMapping("/fallback")
    public String fallbackget() {
        return feignFallBackService.get("a", "b");
    }

    @RequestMapping("/fallbackFactory")
    public String fallbackFactoryGet() {
        return feignFallBackFactoryService.get("a", "b");
    }

}
