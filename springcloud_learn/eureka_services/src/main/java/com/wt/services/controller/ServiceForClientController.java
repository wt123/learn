package com.wt.services.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

/**
 * @Description
 * @Author:wangtao
 * @Date:8:34 2017/6/2
 * @Email:tao8.wang@changhong.com
 */
@Controller
public class ServiceForClientController {

    @Autowired
    private RestTemplate restTemplate;
    final String SERVICE_NAME = "eurake-client-name";

    @RequestMapping("/")
    @ResponseBody
    public String test() {
        String s = restTemplate.getForObject("http://" + SERVICE_NAME + "/test", String.class);
        return "haha" + s;
    }

    @RequestMapping("/test")
    @ResponseBody
    public String aaa() {
        return "services response from 8762";
    }
}
