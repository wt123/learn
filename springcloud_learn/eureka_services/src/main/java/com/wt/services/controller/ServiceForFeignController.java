package com.wt.services.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Description
 * @Author: wangtao
 * @Date:13:50 2017/6/7
 * @Email:tao8.wang@changhong.com
 */
@Controller
@RequestMapping("/servicesForFeign")
public class ServiceForFeignController {

    @RequestMapping(value = "/pathData/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String testFeign(@PathVariable("id") String id) {
        return "id=" + id;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public String get(@RequestParam("key1") String value1, @RequestParam("key2") String value2) {
        return "key1=" + value1 + ";key2=" + value2;
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    @ResponseBody
    public String post(@RequestBody String text) {
        return "result:" + text;
    }

}
