package com.wt.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description
 * @Author:wangtao
 * @Date:13:20 2017/5/31
 * @Email:tao8.wang@changhong.com
 */
@Controller
public class TestController {

    @Value("${person.name}")
    String name;
    @Value("${a.b}")
    String b;
    @RequestMapping("/")
    @ResponseBody
    public String home() {
        return "Hello World!" + name + b;
    }
}
