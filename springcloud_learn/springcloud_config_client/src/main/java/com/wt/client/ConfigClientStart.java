package com.wt.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description
 * @Author:wangtao
 * @Date:10:13 2017/5/31
 * @Email:tao8.wang@changhong.com
 */
@SpringBootApplication
public class ConfigClientStart {

    public static void main(String[] args) {
        SpringApplication.run(ConfigClientStart.class, args);
    }


}