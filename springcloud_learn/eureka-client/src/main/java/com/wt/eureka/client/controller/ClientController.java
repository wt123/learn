package com.wt.eureka.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @Description
 * @Author:wangtao
 * @Date:17:17 2017/6/2
 * @Email:tao8.wang@changhong.com
 */
@RestController
public class ClientController {
    @Autowired
    private DiscoveryClient dc;
    @Autowired
    private RestTemplate restTemplate = new RestTemplate();
    final String SERVICE_NAME = "eurake-services-name";

    @RequestMapping("/allServices")
    public String allServices() {
        List<String> ls = dc.getServices();
        List<ServiceInstance> sis = null;
        for (String s : ls) {
            System.out.println(s);
            sis = dc.getInstances(s);
            for (ServiceInstance si : sis) {
                System.out.println(si.getHost() + "," + si.getServiceId() + ","
                        + si.getMetadata() + "," + si.getPort() + "," + si.getUri() + "," + si.getClass());
            }
            System.out.println("-------------------------------------------");
        }
        return "Hello world";
    }

    @RequestMapping("/test")
    public String discoveryClient() {
        //当前应用是发布在8763端口，调用注册中心的服务eurakeservicesName，可以用localhost:8762/test访问
        String s = restTemplate.getForObject("http://" + SERVICE_NAME + "/test", String.class);
        return s;
    }
}
