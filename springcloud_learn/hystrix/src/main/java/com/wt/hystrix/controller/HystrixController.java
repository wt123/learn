package com.wt.hystrix.controller;

import com.wt.hystrix.integration.SimpleStoreService;
import com.wt.hystrix.integration.TimeOutStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author: wangtao
 * @Date:11:11 2017/6/6
 * @Email:tao8.wang@changhong.com
 */
@Controller
public class HystrixController {

    @Autowired
    private SimpleStoreService simple;

    @Autowired
    private TimeOutStoreService timeOut;

    private static Map<String, Object> map = new HashMap<>();

    static {
        map.put("key", "mapValue");
    }

    //测试普通异常导致的断路器工作
    @RequestMapping("/simple")
    @ResponseBody
    public Object simple() {
        return simple.getStores(map);
    }

    //测试请求超时导致的断路器工作
    @RequestMapping("/timeOut")
    @ResponseBody
    public Object timeOut() {
        return timeOut.getStores(map);
    }
}
