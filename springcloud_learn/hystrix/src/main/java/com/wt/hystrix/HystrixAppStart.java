package com.wt.hystrix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @Description
 * @Author: wangtao
 * @Date:10:30 2017/6/6
 * @Email:tao8.wang@changhong.com
 */
@SpringBootApplication
@EnableCircuitBreaker
@EnableHystrixDashboard
public class HystrixAppStart {
    public static void main(String[] args) {
        SpringApplication.run(HystrixAppStart.class, args);
    }
}
