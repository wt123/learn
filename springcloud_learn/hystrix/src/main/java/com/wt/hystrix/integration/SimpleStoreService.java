package com.wt.hystrix.integration;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Random;

/**
 * @Description
 * @Author: wangtao
 * @Date:11:09 2017/6/6
 * @Email:tao8.wang@changhong.com
 */
@Service
public class SimpleStoreService {

    @HystrixCommand(fallbackMethod = "defaultStores")
    public Object getStores(Map<String, Object> parameters) {
        System.out.println("getStores------->" + parameters.get("key"));
        hystrixRadomError("getStores", "defaultStores");
        return "getStores";
    }

    @HystrixCommand(fallbackMethod = "mydefaultStores")
    public Object defaultStores(Map<String, Object> parameters) {
        System.out.println("defaultStores------->" + parameters.get("key"));
        hystrixRadomError("defaultStores", "mydefaultStores");
        return "defaultStores";
    }

    public Object mydefaultStores(Map<String, Object> parameters) {
        System.out.println("mydefaultStores------->" + parameters.get("key"));
        return "mydefaultStores";
    }

    //让方法报错，进入断路器
    private void hystrixRadomError(String currentMthodName, String nextMethodName) {
        Random random = new Random();
        int i = random.nextInt(10);
        if (i <= 5)
            //i<=5的时候正常
            System.out.println(currentMthodName + ";i=" + i + ";" + currentMthodName + "断路器未工作");
        else {
            //其他情况下，让其出错，执行断路器指定的方法
            System.out.println(currentMthodName + ";i=" + i + ";" + currentMthodName + "断路器开始工作,执行断路器指定的方法：" + nextMethodName);
            i = 1 / 0;
        }
    }

}
