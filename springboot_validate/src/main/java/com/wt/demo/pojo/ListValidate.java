package com.wt.demo.pojo;

import com.wt.demo.validateannotation.MyAnnotation;

import java.util.List;

/**
 * @Description
 * @Author: wangtao
 * @Date:15:05 2017/9/8
 * @Email:tao8.wang@changhong.com
 */
public class ListValidate {

    @MyAnnotation(message = "not be null or empty!!")
    private List<ValidateEntity> ves;

    public List<ValidateEntity> getVes() {
        return ves;
    }

    public void setVes(List<ValidateEntity> ves) {
        this.ves = ves;
    }
}
