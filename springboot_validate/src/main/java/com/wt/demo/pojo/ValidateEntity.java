package com.wt.demo.pojo;

import com.wt.demo.validategroup.ValidateA;
import com.wt.demo.validategroup.ValidateB;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.validation.constraints.NotNull;

/**
 * @Description
 * @Author:wangtao
 * @Date:15:22 2017/5/27
 * @Email:tao8.wang@changhong.com
 */
@PropertySource(value = "classpath:validationMessageSource.properties")
public class ValidateEntity {

    @NotBlank(message = "{name.null}", groups = ValidateA.class)
    private String name;
    @NotNull(message = "年龄不能为空！！！", groups = ValidateB.class)
    private Integer age;
    @NotBlank(message = "grade不能为空！！！", groups = {ValidateA.class, ValidateB.class})
    private String grade;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "ValidateEntity{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", grade='" + grade + '\'' +
                '}';
    }
}
