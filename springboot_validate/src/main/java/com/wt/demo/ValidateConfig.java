package com.wt.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import java.util.Properties;

/**
 * @Description
 * @Author:wangtao
 * @Date:16:19 2017/5/27
 * @Email:tao8.wang@changhong.com
 */
@Configuration
public class ValidateConfig {

    @Value("${app.name.null}")
    private String nameNull;

    @Bean
    public LocalValidatorFactoryBean getLocalValidatorFactoryBean() {
        System.out.println("nameNull:>>>>>>>>>>>" + nameNull);
        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        ReloadableResourceBundleMessageSource ms = new ReloadableResourceBundleMessageSource();
        ms.setBasename("classpath:validateSource");
//        Properties p = new Properties();
//        //取编码的时候从properties属性中取，名字为文件名，所以这里放编码格式进去的时候也要用文件名作为key，
//        //即setBasename的参数
//        p.put("classpath:validateSource", "utf-8");
//        ms.setFileEncodings(p);
        //如果没有像上面一样指定每个配置文件使用的编码时，就使用默认编码，默认为ISO-8859-1，
        //这里设置成UTF-8的话就不用向上面一样单独指定每个配置文件读取的编码方式了
        ms.setDefaultEncoding("UTF-8");
        localValidatorFactoryBean.setValidationMessageSource(ms);
        return localValidatorFactoryBean;
    }

}
