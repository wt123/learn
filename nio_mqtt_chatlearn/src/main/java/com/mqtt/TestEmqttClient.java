package com.mqtt;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;
import java.util.Scanner;

/**
 * @description
 * @author: wangtao
 * @date:15:53 2018/12/6
 * @email:taow02@jumei.com
 */
public class TestEmqttClient {

	public static void main(String[] args) {
		ClientConnectClz.connect("client");
		new Thread(() -> {
			while (true) {
				try {
					while (true) {
						Scanner scanner = new Scanner(System.in);
						String next = scanner.next();
						ClientConnectClz.getClient("client").publish("admin", new MqttMessage(next.getBytes("UTF-8")));
					}
				} catch (MqttException e) {
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}).start();

	}


}

