package com.mqtt;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;

/**
 * @description
 * @author: wangtao
 * @date:15:53 2018/12/6
 * @email:taow02@jumei.com
 */
public class TestEmqtt {

	public static void main(String[] args) {
		ClientConnectClz.connect("client");
		ClientConnectClz.connect("admin");
		new Thread(() -> {
			while (true){
				try {
					Thread.sleep(2000);
					ClientConnectClz.getClient("client").publish("admin",new MqttMessage("我是 client".getBytes("UTF-8")));
				} catch (MqttException e) {
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();

		new Thread(() -> {
			while (true){
				try {
					Thread.sleep(2000);
					ClientConnectClz.getClient("admin").publish("client",new MqttMessage("我是 admin".getBytes("UTF-8")));
				} catch (MqttException e) {
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

}

