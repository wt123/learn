package com.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * @description
 * @author: wangtao
 * @date:17:59 2018/12/6
 * @email:taow02@jumei.com
 */
public class DefaultMqttCallback implements MqttCallback {

	private String clientId;

	public DefaultMqttCallback(String clientId) {
		this.clientId = clientId;
	}

	@Override
	public void connectionLost(Throwable throwable) {
		ClientConnectClz.connect(clientId);//重连一个
	}

	@Override
	public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
		System.out.println("i am " + s + ":i received msg->" + mqttMessage.toString());
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
		System.out.println(iMqttDeliveryToken.toString());
	}
}
