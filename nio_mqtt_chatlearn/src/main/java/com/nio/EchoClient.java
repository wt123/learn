package com.nio;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;

/**
 * 聊天person1
 * @author: wangtao
 * @date:18:06 2018/12/20
 * @email:taow02@jumei.com
 */
public class EchoClient {
	private final String host;
	private final int port;

	public EchoClient(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public void start() throws Exception {
		EventLoopGroup group = new NioEventLoopGroup(1);
		try {
			Bootstrap b = new Bootstrap(); //1
			b.group(group) //2
					.channel(NioSocketChannel.class) //3
					.option(ChannelOption.SO_KEEPALIVE, true)
					.remoteAddress(new InetSocketAddress(host, port)) //4
					.handler(new ChannelInitializer<SocketChannel>() { //5
						@Override
						public void initChannel(SocketChannel ch) throws Exception {
							ch.pipeline().addLast(new EchoClientHandler("wt1:wt2"));
						}
					});
			ChannelFuture f = b.connect().sync(); //6
			f.channel().closeFuture().sync(); //7
			System.out.println("aaa");
		} finally {
//			group.shutdownGracefully().sync(); //8
		}
	}

	public static void main(String[] args) throws Exception {
		if (args.length != 2) {
			args = new String[]{"127.0.0.1", "23456"};
		}
		final String host = args[0];
		final int port = Integer.parseInt(args[1]);
		new EchoClient(host, port).start();
	}
}