package com.nio;

import io.netty.channel.ChannelHandlerContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @description
 * @author: wangtao
 * @date:16:07 2018/12/25
 * @email:taow02@jumei.com
 */
public class CtxManager {

	private static Map<String, ChannelHandlerContext> ctxMap = new HashMap<>();

	public static void addCtx(String key, ChannelHandlerContext ctx) {
		if (!ctxMap.containsKey(key)) {
			ctxMap.put(key, ctx);
		}
	}

	public static ChannelHandlerContext getCtx(String key) {
		return ctxMap.get(key);
	}

}
