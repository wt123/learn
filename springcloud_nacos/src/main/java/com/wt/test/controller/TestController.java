package com.wt.test.controller;

//import com.wt.myaop.service.MyAopService;

import com.wt.test.service.UserEntityService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description
 * @author: wangtao
 * @date:20:27 2019/6/3
 * @email:386427665@qq.com
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Resource
    private UserEntityService userEntityService;

    @RequestMapping("/test")
    public String test(){
        return "test success";
    }

    @RequestMapping("/save")
    public Object save(){
        return userEntityService.getUsers();
    }

}
