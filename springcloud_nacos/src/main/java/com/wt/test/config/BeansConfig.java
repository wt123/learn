package com.wt.test.config;

import com.wt.test.service.TestService;
import com.wt.test.service.TestServiceImpl;
import com.wt.test.service.UserEntityService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class BeansConfig {

    @Bean
    public TestService testService(UserEntityService userEntityService, @Value("${a.b:kk}") String xx) {
        TestService testService = new TestServiceImpl();
        testService.setUserEntityService(userEntityService);
        testService.setXx(xx);
        return testService;
    }

}
