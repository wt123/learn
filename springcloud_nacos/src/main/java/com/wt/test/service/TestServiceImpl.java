package com.wt.test.service;

public class TestServiceImpl implements TestService {

    private UserEntityService userEntityService;

    private String xx;

    @Override
    public void setXx(String xx) {
        this.xx = xx;
    }

    @Override
    public String getXx() {
        return xx;
    }

    @Override
    public UserEntityService getUserEntityService() {
        return userEntityService;
    }

    @Override
    public void setUserEntityService(UserEntityService userEntityService) {
        this.userEntityService = userEntityService;
    }
}
