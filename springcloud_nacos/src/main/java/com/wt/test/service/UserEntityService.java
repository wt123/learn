package com.wt.test.service;

import java.util.List;

/**
 * @description
 * @author: wangtao
 * @date:10:55 2019/6/4
 * @email:386427665@qq.com
 */
public interface UserEntityService {

    List<String> getUsers();

}
