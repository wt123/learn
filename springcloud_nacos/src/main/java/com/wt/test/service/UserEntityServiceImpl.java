package com.wt.test.service;

import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * @description
 * @author: wangtao
 * @date:10:55 2019/6/4
 * @email:386427665@qq.com
 */
@Service
public class UserEntityServiceImpl implements UserEntityService {

    @Override
    public List<String> getUsers() {
        return Collections.singletonList("wt");
    }

}
